package Player;

import final_prj.*;
import java.io.FileInputStream;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.SampleBuffer;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;

// REVIEW: the audio device should not be opened until the
// first MPEG audio frame has been decoded. 
public class LillePlayer {

    /**
     * The current frame number.
     */
    private int frame = 0;

    private String path;

    /**
     * The MPEG audio bitstream.
     */
    // javac blank final bug. 
    /*final*/ private Bitstream bitstream;

    /**
     * The MPEG audio decoder.
     */
    /*final*/ private Decoder decoder;

    /**
     * The AudioDevice the audio samples are written to.
     */
    private AudioDevice audio;

    /**
     * Has the player been closed?
     */
    private boolean closed = false;

    /**
     * Has the player played back all frames from the stream?
     */
    private boolean complete = false;

    private int lastPosition = 0;

    private boolean playing = false;

    private float ms_per_frame = 0;

    private int duration = 0;

    private JPanel seekCursor;
    private JPanel seekControl;
    private JLabel currentTime;
    private int seekControlWidth;

    /**
     * Creates a new <code>LillePlayer</code> instance.
     */
//    public LillePlayer(String path) throws JavaLayerException {
//        this(path, null);
//    }
    public LillePlayer(String path, AudioDevice device, JPanel seekCursor, JLabel currentTime) throws JavaLayerException {
        this.seekCursor = seekCursor;
        this.currentTime = currentTime;
        this.seekControl = (JPanel) seekCursor.getParent();
        seekControlWidth = seekControl.getWidth();
        try {
            this.path = path;
            bitstream = new Bitstream(new FileInputStream(path));
        } catch (Exception ex) {
            throw new JavaLayerException("Exception file not found", ex);
        }
        decoder = new Decoder();

        if (device != null) {
            audio = device;
        } else {
            FactoryRegistry r = FactoryRegistry.systemRegistry();
            audio = r.createAudioDevice();
        }
        audio.open(decoder);

        try {
            do {
                Header h = bitstream.readFrame();
                if (h == null) {
                    break;
                }
                frame++;
                ms_per_frame = h.ms_per_frame();
                duration = (int) (frame * ms_per_frame);

                bitstream.closeFrame();
            } while (true);

            //reset file
            bitstream.close();
            bitstream = new Bitstream(new FileInputStream(path));
            frame = 0;
        } catch (Exception ex) {
            throw new JavaLayerException("Exception initialization", ex);
        }

    }

    public void play() throws JavaLayerException {
        play(Integer.MAX_VALUE);
    }

    /**
     * Plays a number of MPEG audio frames.
     *
     * @param frames	The number of frames to play.
     * @return	true if the last frame was played, or false if there are more
     * frames.
     */
    public boolean play(int frames) throws JavaLayerException {
        boolean ret = true;
        playing = true;
        int count = 0;
        while (frames-- > 0 && ret) {
            if (this.playing) {

                if (count == 10) {
                    seekControl.repaint();
                    seekCursor.setSize((int) (getPosition() * seekControlWidth) / duration, 60);
                    seekControl.repaint();

                    int time = (int) (ms_per_frame * frame) / 1000;

                    int minute = time / 60;
                    int second = time % 60;
                    currentTime.setText(minute + ":" + second);
                    count=0;
                }
                count++;
                seekControl.repaint();
                ret = decodeFrame();
            } else {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
            }
        }

        if (!ret) {
            // last frame, ensure all data flushed to the audio device. 
            AudioDevice out = audio;
            if (out != null) {
                out.flush();
                synchronized (this) {
                    close();
                }
            }
        }
        complete = true;
        return ret;
    }

    /**
     * skips over a single frame
     *
     * @return false	if there are no more frames to decode, true otherwise.
     */
    protected boolean skipFrame() throws JavaLayerException {
        Header h = bitstream.readFrame();
        if (h == null) {
            return false;
        }
        frame++;
        bitstream.closeFrame();
        return true;
    }

    /**
     * Plays a range of MPEG audio frames
     *
     * @param start	The first frame to play
     * @param end	The last frame to play
     * @return true if the last frame was played, or false if there are more
     * frames.
     */
    public boolean play(final int start, final int end) throws JavaLayerException {
        boolean ret = true;
        int offset = start;
        while (offset-- > 0 && ret) {
            ret = skipFrame();
        }
        return play(end - start);
    }

    /**
     * Cloases this player. Any audio currently playing is stopped immediately.
     */
    public synchronized void close() {
        AudioDevice out = audio;
        if (out != null) {
            closed = true;
            complete = true;
            audio = null;
            // this may fail, so ensure object state is set up before
            // calling this method. 
            out.close();
            lastPosition = out.getPosition();
            try {
                bitstream.close();
            } catch (BitstreamException ex) {
            }
        }
        frame = 0;
    }

    /**
     * Returns the completed status of this player.
     *
     * @return	true if all available MPEG audio frames have been decoded, or
     * false otherwise.
     */
    public synchronized boolean isComplete() {
        return complete;
    }

    /**
     * Pause or resume the player.
     *
     */
    public void pause() {
        this.playing = !this.playing;
    }

    public boolean isPause(){
        return !this.playing;
    }
    
    /**
     * Return the volume of the current audio sample being played.
     */
    public synchronized float getVolume() {
        if (audio instanceof JavaSoundAudioDevice) {
            JavaSoundAudioDevice jsAudio = (JavaSoundAudioDevice) audio;
            return jsAudio.getLineGain();
        }
        return 0;
    }

    /**
     * Set the volume of the current audio sample being played.
     */
    public synchronized void setVolume(float gain) {
        if (audio instanceof JavaSoundAudioDevice) {
            JavaSoundAudioDevice jsAudio = (JavaSoundAudioDevice) audio;
            jsAudio.setLineGain(gain);
        }
    }

    /**
     * Retrieves the position in milliseconds of the current audio sample being
     * played. This method delegates to the <code>
     * AudioDevice</code> that is used by this player to sound the decoded audio
     * samples.
     */
    public int getPosition() {
        return (int) (frame * ms_per_frame);
    }

    /**
     * Set the position in milliseconds of the current audio sample being
     * played.
     */
    public synchronized void setPosition(int pos) {
        if (audio == null) {
            return;
        }
        float computePos = 0;

        try {
            if (Math.abs(pos - getPosition()) / 1000 <= 1) {
                return;
            }

            playing = false;

            if ((pos / 1000) < (getPosition() / 1000)) {
                //reset file
                bitstream.close();
                bitstream = new Bitstream(new FileInputStream(path));
                decoder = new Decoder();
                audio.open(decoder);
                frame = 0;
            }

            if (pos < 0 || pos > duration) {
                return;
            }
            do {
                bitstream.readFrame();
                frame++;
                computePos = frame * ms_per_frame;
                bitstream.closeFrame();
            } while (computePos < pos);

            playing = true;
        } catch (Exception ex) {
            System.out.println("ex: " + ex);
        }
    }

    /**
     * Return the duration of title.
     *
     * @return duration in milliseconds.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Decodes a single frame.
     *
     * @return true if there are no more frames to decode, false otherwise.
     */
    protected boolean decodeFrame() throws JavaLayerException {
        
        try {
            AudioDevice out = audio;
            if (out == null) {
                return false;
            }

            Header h = bitstream.readFrame();
            frame++;

            if (h == null) {
                return false;
            }
            SampleBuffer output = (SampleBuffer) decoder.decodeFrame(h, bitstream);
            synchronized (this) {
                // sample buffer set when decoder constructed
                

                out = audio;
                if (out != null) {
                    out.write(output.getBuffer(), 0, output.getBufferLength());
                }
            }

            bitstream.closeFrame();
        } catch (Exception ex) {
            System.out.println("Decoding exception: ending...");
            if (bitstream != null) {
                bitstream.closeFrame();
            }

            return false;
        }
        /*catch (RuntimeException ex)
		 {
		 throw new JavaLayerException("Exception decoding audio frame", ex);
		 }*/
        return true;
    }

    public float getMS(){
        return ms_per_frame;
    }
}
