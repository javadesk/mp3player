package Player;

import SongManagement.Song;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class NowPlaying {

    ArrayList<Song> nowPlaying;
    int itterator = -1;
    private boolean isUsed = false;

    public NowPlaying() {
        nowPlaying = new ArrayList<>();
    }

    public int size() {
        return nowPlaying.size();
    }

    public boolean isEmpty() {
        return nowPlaying.isEmpty();
    }

    synchronized public Song next() {
        if (itterator >= nowPlaying.size() - 1) {
            return null;
        }
        itterator++;
        return nowPlaying.get(itterator);
    }

    synchronized public Song prev() {

        if (itterator <= 0) {
            return null;
        }
        itterator--;
        return nowPlaying.get(itterator);
    }

    public Song getSong(int index) {
        return nowPlaying.get(index);
    }

    synchronized public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean status) {
        isUsed = status;
    }

    public void setList(ArrayList<Song> list) {
        nowPlaying = list;
        itterator = -1;

    }

    synchronized public void add(Song song) {
        nowPlaying.add(song);
    }

    public ArrayList<Song> getList() {
        return nowPlaying;
    }

    public void load(ArrayList<Song> playlist) {
        nowPlaying = playlist;
    }

    public ArrayList<Song> shuff() {

        ArrayList<Song> temp = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        ArrayList<Integer> index=new ArrayList<>();
        int k=0;
        if(itterator>=0){
            k=1;
            temp.add(nowPlaying.get(itterator));
        }

        for(int i=0; i<nowPlaying.size(); i++){
            if(itterator!=i){
                index.add(i);
            }
            
        }

        while(!index.isEmpty()){
            int j = (int) (Math.random() * (index.size() - 1));
            temp.add(nowPlaying.get(index.get(j)));
            index.remove(j);
        }
        return temp;
    }

    public void shuffer() {
        nowPlaying = shuff();
        itterator = -1;
    }
    
    public void setItterator(int x){
        itterator=x;
    }
    
    public Song getCurrentSong(){
        return nowPlaying.get(itterator);
    }
}
