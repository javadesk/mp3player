/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SongManagement;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.imageio.ImageIO;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import sun.awt.image.ImageFormatException;

/**
 *
 * @author myfar
 */
public class Song implements Serializable {

    private String songName;
    private String artist;
    private String composer;
    private String genre;
    private String album;
    private byte[] image;
    private String path;
    private boolean getSuccess = true;

    public Song(String path) {
        this.path = path;
        getSuccess = getMeta();
    }

    protected final boolean getMeta() {
        String fileLocation = path;
        try {
            InputStream input = new FileInputStream(new File(fileLocation));
            ContentHandler handler = new DefaultHandler();
            Metadata metadata = new Metadata();
            Parser parser = new Mp3Parser();
            ParseContext parseCtx = new ParseContext();
            parser.parse(input, handler, metadata, parseCtx);
            input.close();
            // List all metadata
//            String[] metadataNames = metadata.names();
//            for (String name : metadataNames) {
//                System.out.println(name + ": " + metadata.get(name));
//            }

            // Retrieve the necessary info from metadata
            // Names - title, xmpDM:artist etc. - mentioned below may differ based
            try {
                songName = metadata.get("title").trim();
            } catch (NullPointerException e) {
                songName = "unknown";
            }

            try {
                artist = metadata.get("xmpDM:artist").trim();

            } catch (NullPointerException e) {
                artist = "unknown";
            }

            try {
                composer = metadata.get("xmpDM:composer").trim();

            } catch (NullPointerException e) {
                composer = "unknown";
            }
            try {
                genre = metadata.get("xmpDM:genre").trim();

            } catch (NullPointerException e) {
                genre = "unknown";
            }
            try {
                album = metadata.get("xmpDM:album").trim();

            } catch (NullPointerException e) {
                album = "unknown";
            }

        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException | SAXException | TikaException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        try {
            String srcFile = path;
            Mp3File song = new Mp3File(srcFile);
            if (song.hasId3v2Tag()) {
                ID3v2 id3v2tag = song.getId3v2Tag();
                byte[] imageData = id3v2tag.getAlbumImage();

                image = imageData;
                //converting the bytes to an image
//                BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageData));
//                File outputfile = new File("image.jpg");
//                ImageIO.write(img, "jpg", outputfile);
            } else {
                image = null;
            }
        } catch (Exception ex) {
            System.out.println("ex: " + ex);
            image = null;
        }
        return true;
    }

    public String getSongName() {
        return songName;
    }

    public String getArtist() {
        return artist;
    }

    public String getComposer() {
        return composer;
    }

    public String getGenre() {
        return genre;
    }

    public String getAlbum() {
        return album;
    }

    public byte[] getImage() {
        return image;
    }

    public String getPath() {
        return path;
    }

    public boolean isSuccess() {
        return getSuccess;
    }
}
