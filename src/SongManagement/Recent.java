package SongManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Recent implements Serializable{

    private ArrayList<Song> list;

    public Recent() {
        list = new ArrayList<>();
    }

    public int size() {
        return list.size();
    }

    public void add(Song e) {
        for(int i=0; i<list.size(); i++){
            if(e.getPath().equals(list.get(i).getPath())){
                return;
            }
        }
        while (list.size() >= 10) {
            list.remove(0);
        }
        list.add(e);
    }

    public Song getSong(int index) {
        return list.get(index);
    }
    
    public ArrayList<Song> getList(){
        return list;
    }

    public void verify(){
        for(int i=0; i<list.size(); i++){
            if(!(new File(list.get(i).getPath())).exists()){
                list.remove(i);
                i--;
            }
        }
    }
    
    public void save(String filename) throws FileNotFoundException, IOException {
        File f = new File(filename);
        if (f.exists()) {
            f.delete();
        }
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        }
    }

    public void load(String filename) throws FileNotFoundException, ClassNotFoundException, IOException{
        if(!new File(filename).exists()) return;
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            list = (ArrayList<Song>) ois.readObject();
            verify();
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException | ClassNotFoundException ex) {
            throw ex;
        }
    }
}
