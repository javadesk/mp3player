package SongManagement;

import Help.ProcessText;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SongManagement {

    ArrayList<Song> list;

    public SongManagement() {
        list = new ArrayList<>();
    }

    public int size() {
        return list.size();
    }

    public void scanFolderForMp3(String[] paths) {
        if (paths.length == 0) {
            return;
        }
        for (String path : paths) {
            extract(path);
        }

    }

    public void remove(String path){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getPath().contains(path)){
                list.remove(i);
                i--;
            }
        }
    }

    public Song get(int index) {
        return list.get(index);
    }

    public ArrayList<Song> searchSong(String keyWord) {
        ArrayList<Song> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSongName().contains(keyWord)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public ArrayList<Song> searchArtist(String keyWord) {
        ArrayList<Song> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getArtist().contains(keyWord)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public ArrayList<Song> searchAlbum(String keyWord) {
        ArrayList<Song> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAlbum().contains(keyWord)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public ArrayList<Song> searchGenre(String keyWord) {
        if (keyWord.equalsIgnoreCase("All")) {
            return listAllSong();
        }
        ArrayList<Song> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getGenre().contains(keyWord)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public ArrayList<Song> searchAll(String keyWord) {
        keyWord = ProcessText.unAccent(keyWord.toLowerCase());
        ArrayList<Song> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Song temp = list.get(i);
            if (ProcessText.unAccent(temp.getSongName().toLowerCase()).contains(keyWord)
                    || ProcessText.unAccent(temp.getArtist().toLowerCase()).contains(keyWord)
                    || ProcessText.unAccent(temp.getAlbum().toLowerCase()).contains(keyWord)) {
                result.add(temp);
            }
        }
        return result;
    }

    public ArrayList<Song> listAllSong() {
        return list;
    }

    public Set<String> listAllArtist() {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < list.size(); i++) {
            set.add(list.get(i).getArtist());
        }
        return set;
    }

    public Set<String> listAllAlbum() {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < list.size(); i++) {
            set.add(list.get(i).getAlbum());
        }
        return set;
    }

    public Set<String> listAllGenre() {
        Set<String> set = new HashSet<>();
        set.add("All");
        for (int i = 0; i < list.size(); i++) {
            set.add(list.get(i).getGenre());
        }
        return set;
    }

    public void extract(String p) {
        File f = new File(p);
        File l[] = f.listFiles();
        for (File x : l) {
            if (x == null) {
                return;
            }
            if (x.isHidden() || !x.canRead()) {
                continue;
            }
            if (x.isDirectory()) {
                extract(x.getPath());
            } else if (x.getName().endsWith(".mp3")) {
                
                Song song = new Song(x.getPath());
                if (song.isSuccess()) {
                    list.add(song);
                }

            }
        }
    }
}
