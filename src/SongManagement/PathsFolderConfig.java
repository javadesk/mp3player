package SongManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PathsFolderConfig implements Serializable{

    Set<String> paths;

    public PathsFolderConfig() {
        paths = new HashSet<>();
    }

    public void add(String path) {
        paths.add(path);
    }

    public int size(){
        return paths.size();
    }
    
    public String[] getListFolders(){
        return paths.toArray(new String[paths.size()]);
    }
    
    public String[] toArray() {
        if(paths.isEmpty()) return new String[0];
        return (String[]) paths.toArray(new String[paths.size()]);
    }

    public void remove(String path){
        paths.remove(path);
    }
    
    public void save(String filename) throws FileNotFoundException, IOException {
        File f = new File(filename);
        if (f.exists()) {
            f.delete();
        }
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(paths);
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        }
    }

    public void load(String filename) throws FileNotFoundException, ClassNotFoundException, IOException {
        if(!new File(filename).exists()) return;
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            paths = (Set<String>) ois.readObject();
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException | ClassNotFoundException ex) {
            throw ex;
        }
    }
}
