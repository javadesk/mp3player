
package SongManagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PlayList implements Serializable{
    HashMap<String, ArrayList<Song>> map;

    public PlayList() {        
        map=new HashMap<>();
    }
    
    public int size(){
       return map.size();
    }
    
    public Set<String> keySet(){
        return map.keySet();
    }
    
    public boolean isEmpty(){
        return map.isEmpty();
    }
    
    public ArrayList<Song> getSongInPlaylist(String playlist){
        return map.get(playlist);
    }
    
    public Set<String> getPlaylist(){
        return map.keySet();
    }
    
    public void addSong(Song song, String playlist){
        ArrayList<Song> temp;
        if(map.containsKey(playlist)){
            temp=map.get(playlist);
            for(int i=0; i<temp.size(); i++){
                if(song.equals(temp.get(i))){
                    return;
                }
            }
            temp.add(song);
        }else{
            temp=new ArrayList<>();
            temp.add(song);
        }
        map.put(playlist, temp);
    }
    
    public void removeSong(int index, String playlist){
        ArrayList<Song> temp=map.get(playlist);
        temp.remove(index);
        map.put(playlist, temp);
    }
    
    public void addPlaylist(String playlist){
        if(map.containsKey(playlist)) return;
        map.put(playlist, new ArrayList<>());
    }
    
    public void removePlaylist(String playlist){
        map.remove(playlist);
    }
    
    protected void verify(){
        String[] keys=(String[]) map.keySet().toArray(new String[map.size()]);
        
        for(int i=0; i<keys.length; i++){
            ArrayList<Song> songs=map.get(keys[i]);
            for(int j=0; j<songs.size(); j++){
                if(!(new File(songs.get(j).getPath())).exists()){
                    songs.remove(j);
                    
                }
            }
            map.put(keys[i], songs);
        }
    }   
    
    public void save(String filename) throws FileNotFoundException, IOException{
        File f=new File(filename);
        if(f.exists()){
            f.delete();
        }
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos=new ObjectOutputStream(fos);
            oos.writeObject(map);
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        }
    }
    
    public void load(String filename) throws FileNotFoundException, ClassNotFoundException, IOException{
        if(!new File(filename).exists()) return;
        try{
            FileInputStream fis=new FileInputStream(filename);
            ObjectInputStream ois=new ObjectInputStream(fis);
            map=(HashMap<String, ArrayList<Song>>) ois.readObject();
            verify();
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException | ClassNotFoundException ex) {
            throw ex;
        }
    }
}
