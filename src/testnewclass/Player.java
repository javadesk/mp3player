package testnewclass;

import Player.*;
import final_prj.*;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.swing.JPanel;
import javazoom.jl.decoder.JavaLayerException;

public class Player {

    private LillePlayer player;
    private int pausePoint;
    private int totalDuration;
    private float widthPerSecond;

    public void play(String filePath, int widthSeek) {
        try {
            File audioFile = new File(filePath);
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioInputStream din = null;
            AudioFormat baseFormat = audioStream.getFormat();
            AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    baseFormat.getSampleRate(),
                    16,
                    baseFormat.getChannels(),
                    baseFormat.getChannels() * 2,
                    baseFormat.getSampleRate(),
                    false);
            din = AudioSystem.getAudioInputStream(decodedFormat, audioStream);
            JavaSoundAudioDevice device = new JavaSoundAudioDevice();
            device.open(din.getFormat());
            device.createSource();

            player = new LillePlayer(filePath, device);
            totalDuration = player.getDuration() / 1000;
            System.out.println("Duration " + totalDuration);
            widthPerSecond = widthSeek / totalDuration;
            System.out.println("width persecond " + widthPerSecond);
            //chỉnh âm lượng bằng cách dùng hàm setVolume của player, tham số là từ 0f - 1f. ví dụ: player.setVolume(0.5f);
            //chỉnh vị trí phát dùng hàm setPosition của player, tham số từ 0 đến player.getDuration();
            //set am luong 0.5;
            player.setVolume(1f);
//            player.setPosition(5000);
            //player.setPosition(15000);

            new Thread() {
                @Override
                public void run() {
                    try {
                        player.play();
                    } catch (JavaLayerException ex) {
                        System.out.println("e : " + ex);
                    }
                }
            }.start();

        } catch (Exception e) {
            System.out.println("e: " + e);
        }
    }

    public void play(String filePath, JPanel seekCursor) {
        try {
            File audioFile = new File(filePath);
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioInputStream din = null;
            AudioFormat baseFormat = audioStream.getFormat();
            AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    baseFormat.getSampleRate(),
                    16,
                    baseFormat.getChannels(),
                    baseFormat.getChannels() * 2,
                    baseFormat.getSampleRate(),
                    false);
            din = AudioSystem.getAudioInputStream(decodedFormat, audioStream);
            JavaSoundAudioDevice device = new JavaSoundAudioDevice();
            device.open(din.getFormat());
            device.createSource();

            player = new LillePlayer(filePath, device);
            totalDuration = player.getDuration() / 1000;
            System.out.println("Duration " + totalDuration);
            widthPerSecond = seekCursor.getParent().getWidth() / totalDuration;
            System.out.println("width persecond " + widthPerSecond);
            //chỉnh âm lượng bằng cách dùng hàm setVolume của player, tham số là từ 0f - 1f. ví dụ: player.setVolume(0.5f);
            //chỉnh vị trí phát dùng hàm setPosition của player, tham số từ 0 đến player.getDuration();
            //set am luong 0.5;
            player.setVolume(1f);
//            player.setPosition(5000);
            //player.setPosition(15000);

            new Thread() {
                @Override
                public void run() {
                    try {
                        player.play();
                    } catch (JavaLayerException ex) {
                        System.out.println("e : " + ex);
                    }
                }
            }.start();

        } catch (Exception e) {
            System.out.println("e: " + e);
        }
    }

    public void stop() {
        player.close();
    }

    public void pause() {
        player.pause();
        pausePoint = player.getPosition();
    }

    public void resume() {
        player.setPosition(pausePoint);
    }

    public int getWidthSeekCursor() {
        return (int) (widthPerSecond * getPosition());
    }

    public int getPosition() {
        return player.getPosition() / 1000;
    }

    public void setVolume(float f) {
        player.setVolume(f);
    }

    public int getDuration() {
        return player.getDuration() / 1000;
    }

    public void seek(int position) {
        player.setPosition(position * 1000);
    }

    public boolean isComplete() {
        return player.isComplete();
    }
}
