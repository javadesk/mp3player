package Network;

import final_prj.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB_Connection {

    /*public static void main(String[] args) {
        DB_Connection obj_DB_Connection = new DB_Connection();
        Connection connection = null;
        connection = obj_DB_Connection.get_connection();
        System.out.println(connection);
    }*/

    public Connection get_connection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");//com.mysql.jdbc.Driver
            connection = DriverManager.getConnection("jdbc:mysql://db4free.net:3306/prj_project", "prj_final", "123@123a");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        return connection;
    }
}
