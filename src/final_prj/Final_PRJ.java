/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final_prj;

import Player.NowPlaying;
import Player.Player;
import SongManagement.PathsFolderConfig;
import SongManagement.PlayList;
import SongManagement.Recent;
import SongManagement.Song;
import SongManagement.SongManagement;
import com.sun.xml.internal.bind.v2.model.core.Adapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author myfar
 */
public class Final_PRJ extends javax.swing.JFrame {

    /**
     * Creates new form Final_PRJ
     */
    SongManagement songMan = new SongManagement();
    PlayList playlist = new PlayList();
    String playlistPath = "playlist.ser";
    Recent recent = new Recent();
    String recentPath = "recent.ser";
    PathsFolderConfig pathsFolder = new PathsFolderConfig();
    String pathFolder = "pathsFolder.ser";

    NowPlaying nowPlaying = new NowPlaying();
    boolean killNowPlaying = false;
    boolean next = false;
    boolean prev = false;
    boolean repeatStatus = false;
    boolean shuffStatus = false;
    boolean pause = true;
    boolean skip = false;
    boolean loadSongMan = false;
    boolean canReload=false;

    Set<String> setArtist;
    Set<String> setAlbum;

    final Dimension defautGenreMenuDimension = new Dimension(160, 440);
    final Dimension defaultRecentDimension = new Dimension(910, 440);
    final Dimension defaultAlbumDimension = new Dimension(910, 440);
    final Dimension defaultArtistDimension = new Dimension(910, 440);
    final Dimension defaultSuggestionDimension = new Dimension(290, 390);
    final Dimension defaultPlayListSubMenuDimension = new Dimension(120, 120);
    final Dimension defaultSongListDimension = new Dimension(620, 260);
    final Dimension defaultSongListSearchDimension = new Dimension(910, 380);
    final Dimension defaultFolderListDimension = new Dimension(320, 200);
    final Dimension defaultNowPlayingListDimension = new Dimension(100, 130);
    final Dimension defaultGenreSongDimension = new Dimension(750, 440);

    final Color hover = new Color(44, 57, 66);
    final Color hoverImage = new Color(24, 31, 36, 60);
    final Color sidebar_color = new Color(24, 31, 36);
    final Color cursorColor = new Color(76, 182, 203, 90);// 76, 182, 203, 90 - 219, 0, 219, 90 - 110, 219, 0,90
    final Color tranperent = new Color(156, 157, 158, 30);
    final Color theme = new Color(76, 182, 203);
    final Color white = Color.WHITE;
    final Color GenreMenuColor = new Color(224, 234, 236);

    byte[] defaultImageAlbum = null;
    final String pathDefaultImageAlbum = "src/Images/matticonsalbumcover.jpg";
    byte[] defaultImageSong = null;
    final String pathDefaultImageSong = "src/Images/songDefaultImage.png";
    byte[] defaultImageArtist = null;
    final String pathDefaultImageArtist = "src/Images/defaultArtist.png";

    final String genreCol = "genre";
    final String artistNameCol = "artistName";
    final String songName = "songName";
    final String composerNameCol = "composerName";
    final String songImageCol = "songImage";

    boolean isPlaying = false;
    boolean canSeek = false;
    boolean alreadyPlay = false;

    //Playerv1 player = new Playerv1();
    Player player = new Player();
    boolean canResume = false;
    int xMouse, yMouse;
    int markTab = 1;
    ArrayList<Component> extendTab = new ArrayList<>();
    Border borderLeft = BorderFactory.createMatteBorder(0, 4, 0, 0, theme);

    public Final_PRJ() {
        initComponents();
        Loading.setVisible(false);
        createPlaylistContainer.setVisible(false);
        accountInforContainer.setVisible(false);
        searchResultPanel.setVisible(false);
        addFoldersContainer.setVisible(false);
        nowPlayingContainer.setVisible(false);
        infoArtist.setVisible(false);
        playListSubMenuContainer.setVisible(false);
        loadToArray();
        
        transparentPanel.setBackground(tranperent);
        transparentPanel.setVisible(false);
        
        initSideBar();
        
        setupScrollbar();

        try {

            loadConfigFile();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
        }

        

        loadAll();

        //Loading.setIcon(new ImageIcon(this.getClass().getResource("test-test.gif")));
        //Loading.setIcon(new ImageIcon(new ImageIcon("src/Images/Ripple-2.3s-200px.gif").getImage().getScaledInstance(Loading.getWidth(), Loading.getHeight(), Image.SCALE_SMOOTH)));
    }
    

    private void loadAll() {
        new Thread() {
            @Override
            public void run() {
                setAllUnVisible();
                scrollRecentPanel.setVisible(true);
                try {
                    image();
                } catch (IOException ex) {
                    Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                boolean[] done={false, false, false, false, false};
                
                new Thread() {
                    @Override
                    public void run() {
                        //pathsFolder.add("C:\\Users\\myfar\\Desktop\\Musics");
                        songMan=new SongManagement();
                        songMan.scanFolderForMp3(pathsFolder.toArray());
                        loadSongMan = true;
                    }

                }.start();

                new Thread() {
                    @Override
                    public void run() {
                        while (true) {
                            if (scrollPlaylistPanel.isVisible()) {
                                transparentPanel.setVisible(true);
                                Loading.setVisible(true);
                            }

                            loadPlayListContainer(playlistPanel);
                            if (scrollPlaylistPanel.isVisible()) {
                                transparentPanel.setVisible(false);
                                Loading.setVisible(false);
                            }

                            mainpanel.repaint();
                            done[0]=true;
                            break;
                        }
                    }

                }.start();

                new Thread() {
                    @Override
                    public void run() {
                        while (true) {
                            if (scrollAlbumPanel.isVisible()) {
                                transparentPanel.setVisible(true);
                                Loading.setVisible(true);
                            }
                            if (loadSongMan) {
                                setAlbum = songMan.listAllAlbum();
                                loadAlbumContainer(albumPanel, setAlbum);
                                if (scrollAlbumPanel.isVisible()) {
                                    transparentPanel.setVisible(false);
                                    Loading.setVisible(false);
                                }

                                mainpanel.repaint();
                                done[1]=true;
                                break;
                            }
                            try {
                                sleep(200);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }

                }.start();

                new Thread() {
                    @Override
                    public void run() {
                        while (true) {
                            if (scrollArtistPanel.isVisible()) {
                                transparentPanel.setVisible(true);
                                Loading.setVisible(true);
                            }
                            if (loadSongMan) {
                                setArtist = songMan.listAllArtist();
                                loadArtistContainer(ArtistPanel, setArtist);
                                if (scrollArtistPanel.isVisible()) {
                                    transparentPanel.setVisible(false);
                                    Loading.setVisible(false);
                                }

                                mainpanel.repaint();
                                done[2]=true;
                                break;
                            }
                            try {
                                sleep(200);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }

                }.start();

                new Thread() {
                    @Override
                    public void run() {
                        while (true) {
                            if (GenrePanel.isVisible()) {
                                transparentPanel.setVisible(true);
                                Loading.setVisible(true);
                            }
                            if (loadSongMan) {
                                loadGenreMenu(songMan.listAllGenre());
                                loadRecentSongContainer(GenreSongContainer, songMan.listAllSong(), 1);
                                if (GenrePanel.isVisible()) {
                                    transparentPanel.setVisible(false);
                                    Loading.setVisible(false);
                                }

                                mainpanel.repaint();
                                done[3]=true;
                                break;
                            }
                            try {
                                sleep(200);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }

                }.start();

                new Thread() {
                    @Override
                    public void run() {
                        if (scrollRecentPanel.isVisible()) {
                            transparentPanel.setVisible(true);
                            Loading.setVisible(true);
                        }
                        loadRecentSongContainer(RecentPanel, recent.getList(), 0);
                        if (scrollRecentPanel.isVisible()) {
                            transparentPanel.setVisible(false);
                            Loading.setVisible(false);
                        }
                        done[4]=true;
                    }

                }.start();
                /*new Thread() {
                    @Override
                    public void run() {
                        loadArtistContainer(ArtistPanel, setArtist);
                        loadAlbumContainer(albumPanel, setAlbum);
                        loadPlayListContainer(playlistPanel);
                    }

                }.start();*/ //setMouseClickSidebarItems();
                initSeek();
                setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons8_Headphones_32px.png")));

                transparentPanel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        transparentPanel.setVisible(false);
                        createPlaylistContainer.setVisible(false);
                        playListSubMenuContainer.setVisible(false);
                        accountInforContainer.setVisible(false);
                        addFoldersContainer.setVisible(false);
                        nowPlayingContainer.setVisible(false);
                    }

                });
                
                while(true){
                    boolean check=true;
                    for(int i=0; i<done.length; i++){
                        if(!done[i]){
                            check=false;
                            break;
                        }
                    }
                    if(check){
                        canReload=true;
                        break;
                    }
                    try {
                        sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }.start();
        
    }

    private void image() throws IOException {
        defaultImageAlbum = toByteArray(pathDefaultImageAlbum);
        defaultImageSong = toByteArray(pathDefaultImageSong);
        defaultImageArtist = toByteArray(pathDefaultImageArtist);
    }

    private void setupScrollbar() {
        CustomScrollBar.setup(scrollRecentPanel);
        CustomScrollBar.setup(scrollGenrePanel);
        CustomScrollBar.setup(scrollGenreMenuPanel);
        CustomScrollBar.setup(scrollArtistPanel);
        CustomScrollBar.setup(scrollDescriptionArtist);
        CustomScrollBar.setup(scrollOtherAritst);
        CustomScrollBar.setup(scrollSongList);
        CustomScrollBar.setup(scrollAlbumPanel);
        CustomScrollBar.setup(scrollPlayListSubMenu);
        CustomScrollBar.setup(scrollSongListSearchResult);
        CustomScrollBar.setup(scrollBarFoldersList);
        CustomScrollBar.setup(scrollPlaylistPanel);
        CustomScrollBar.setup(scrollNowPlaying);
    }

    private void loadConfigFile() throws ClassNotFoundException, IOException {
        playlist.load(playlistPath);
        recent.load(recentPath);
        pathsFolder.load(pathFolder);
    }

    private void saveConfigFile() throws IOException {
        playlist.save(playlistPath);
        recent.save(recentPath);
        pathsFolder.save(pathFolder);
    }

    private byte[] toByteArray(String path) throws IOException {
        BufferedImage bImage = ImageIO.read(new File(path));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", bos);
        byte[] data = bos.toByteArray();
        return data;
    }

    //note that method is not completed time current playing
    private void play(Song song) {
        if (player.getCheckInit()) {
            player.stop();
        }
        pause = false;
        playButtonPlayerController.setIcon(new ImageIcon("src/Images/icons8_Pause_32px.png"));
        resetSeekController();
        ArtistName_SkipControl.setText(song.getArtist());
        SongName_SkipControl.setText(song.getSongName());

        player = new Player(seekCursor, CurrentTimeOfSkipCursor);
        player.play(song.getPath());
        int minute = player.getDuration() / 60;
        int second = player.getDuration() % 60;
        LengthOfSong.setText(minute + ":" + second);
        float x = (float) volumeCursor.getWidth() / volumeSlider.getWidth();
        player.setVolume(0.4f + ((float) volumeCursor.getWidth() * 0.6f / volumeSlider.getWidth()));
        recent.add(song);

    }

    private void play() {
        if (!nowPlaying.isUsed()) {

            nowPlaying.setUsed(true);
            new Thread() {
                @Override
                public void run() {
                    if (!nowPlaying.isEmpty()) {
                        Song temp = nowPlaying.next();
                        System.out.println(temp.getSongName());
                        outer:
                        while (temp != null) {
                            play(temp);
                            while (!player.isComplete()) {
                                if (next) {
                                    next = false;
                                    temp = nowPlaying.next();
                                    if (temp != null) {
                                        continue outer;
                                    }
                                }
                                if (prev) {
                                    prev = false;
                                    temp = nowPlaying.prev();
                                    if (temp != null) {
                                        continue outer;
                                    }
                                }
                                if (skip) {
                                    skip = false;
                                    temp = nowPlaying.getCurrentSong();
                                    if (temp != null) {
                                        continue outer;
                                    }

                                }
                                if (killNowPlaying) {
                                    killNowPlaying = false;
                                    break;
                                }

                                try {
                                    sleep(1000);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            if (!repeatStatus) {
                                temp = nowPlaying.next();
                            }

                        }

                    }

                    nowPlaying.setUsed(false);
                    killNowPlaying = false;
                    pause = true;
                    playButtonPlayerController.setIcon(new ImageIcon("src/Images/icons8_Play_32px.png"));
                    seekControl.repaint();

                }
            }.start();
        }
    }

    synchronized private boolean pla(Song song) {

        if (!player.getCheckInit()) {
            play(song);
            return true;
        }
        if (song != null && player.getCheckInit() && player.isComplete()) {
            play(song);
            return true;
        }
        return false;
    }

    private void resetSeekController() {
        seekCursor.setSize(0, 60);
        seekControl.repaint();

    }

    private void resetShuffRepeat() {
        shuffStatus = false;
        shuffPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Shuffle_25px.png"));
        repeatStatus = false;
        repeatPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Repeat_25px.png"));
    }

    private void setAllUnVisible() {
        for (Component x : extendTab) {
            x.setVisible(false);
        }
        infoArtist.setVisible(false);
        searchResultPanel.setVisible(false);
    }

    private void loadToArray() {
        extendTab.add(scrollRecentPanel);
        extendTab.add(GenrePanel);
        extendTab.add(scrollArtistPanel);
        extendTab.add(scrollAlbumPanel);
        extendTab.add(scrollPlaylistPanel);
    }

    private void reCalculateLocation(JPanel container) {
        Component[] temp = container.getComponents();
        container.setPreferredSize(new Dimension(320, 400));
        if (temp.length == 0) {
            return;
        }
        int xaxis = 0;
        int yaxis = 0;
        int totalHeightItem = yaxis;
        for (int i = 0; i < temp.length; i++) {

            ((JPanel) temp[i]).setLocation(xaxis, yaxis);

            yaxis += 80;
            totalHeightItem += 80;
        }
        if (totalHeightItem > 400) {
            container.setPreferredSize(new Dimension(320, totalHeightItem));
        }
        container.repaint();

    }

    private void addFolders(JPanel container, String path) {
        int xaxis = 0;
        int yaxis = 0;
        Component[] temp = container.getComponents();
        if (temp.length == 0) {
            yaxis = 0;
        } else {
            yaxis = temp[temp.length - 1].getY() + 80;
        }
        JPanel[] Items = new JPanel[50];

        int totalHeightItem = yaxis;
        int defaultHeightContainer = 400;
        int dedaultWeightContainer = 320;

        JPanel Item = new JPanel(null);
        Item.setSize(320, 80);

        JLabel avatarItem = new JLabel();
        avatarItem.setSize(20, 20);
        avatarItem.setLocation(290, 20);
        avatarItem.setIcon(new ImageIcon(new ImageIcon("src/Images/icons8_Delete_20px.png").getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));
        JLabel nameItem = new JLabel();

        nameItem.setSize(265, 20);
        nameItem.setLocation(10, 40);
        nameItem.setText(path);

        Item.add(avatarItem);
        Item.add(nameItem);

        JPanel layer = new JPanel(null);
        layer.setSize(320, 80);
        layer.setBackground(hoverImage);
        layer.setLocation(0, 0);

        Item.add(layer);
        layer.setVisible(false);

        Item.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                layer.setVisible(true);
                container.repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                layer.setVisible(false);
                container.repaint();
            }

            @Override
            public void mouseClicked(MouseEvent me) {
                container.remove(Item);
                reCalculateLocation(container);
                pathsFolder.remove(path);
                songMan.remove(path);
                setAlbum = songMan.listAllAlbum();
                setArtist = songMan.listAllArtist();

                loadGenreMenu(songMan.listAllGenre());
                loadRecentSongContainer(GenreSongContainer, songMan.listAllSong(), 1);
                loadArtistContainer(ArtistPanel, setArtist);
                loadAlbumContainer(albumPanel, setAlbum);
            }

        });

        Item.setLocation(xaxis, yaxis);

        Item.setVisible(true);
        container.add(Item);
        if (totalHeightItem > defaultHeightContainer) {
            container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
        }

    }

    private void loadGenreMenu() {
        //Component[] GenreMenuCom=GenreMenu.getComponents();
        JPanel[] menulist = new JPanel[20];
        int width = GenreMenu.getWidth();
        int yaxis = 0;
        int defaultHeightMenu = GenreMenu.getHeight();
        int defaultWidthMenu = GenreMenu.getHeight();
        int totalHeightItem = 0;

        for (int i = 0; i < menulist.length; i++) {
            JPanel temp = new JPanel();
            temp.setSize(width, 30);
            temp.setLayout(null);
            JLabel label = new JLabel("text " + i);
            label.setSize(35, 14);
            label.setLocation(20, 8);
            label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            temp.add(label);
            temp.setBackground(GenreMenuColor);

            if (i == 0) {
                yaxis = 0;
            } else {
                yaxis += temp.getHeight();
            }
            temp.setLocation(0, yaxis);
            temp.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    temp.setBackground(cursorColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    temp.setBackground(GenreMenuColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    GenreSongContainer.removeAll();

                    loadSongContainer(GenreSongContainer);

                    GenreSongContainer.repaint();
                }

            });
            GenreMenu.add(temp);
            totalHeightItem += temp.getHeight();
            if (totalHeightItem > defaultHeightMenu) {
                GenreMenu.setPreferredSize(new Dimension(defaultWidthMenu, totalHeightItem));
            }
        }
    }

    private void loadGenreMenu(Set<String> genre) {
        //Component[] GenreMenuCom=GenreMenu.getComponents();
        GenreMenu.removeAll();
        GenreMenu.setPreferredSize(defautGenreMenuDimension);
        if (genre.isEmpty()) {
            return;
        }
        String[] gen = (String[]) genre.toArray(new String[genre.size()]);
        JPanel[] menulist = new JPanel[gen.length];
        int width = GenreMenu.getWidth();
        int yaxis = 0;
        int defaultHeightMenu = GenreMenu.getHeight();
        int defaultWidthMenu = GenreMenu.getHeight();
        int totalHeightItem = 0;

        for (int i = 0; i < gen.length; i++) {
            JPanel temp = new JPanel();
            temp.setSize(width, 30);
            temp.setLayout(null);
            JLabel label = new JLabel(gen[i]);
            label.setSize(width - 20, 14);
            label.setLocation(20, 8);
            label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            temp.add(label);
            temp.setBackground(GenreMenuColor);

            if (i == 0) {
                yaxis = 0;
            } else {
                yaxis += temp.getHeight();
            }
            temp.setLocation(0, yaxis);
            int j = i;
            temp.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    temp.setBackground(cursorColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    temp.setBackground(GenreMenuColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    GenreSongContainer.removeAll();
                    ArrayList<Song> temp = songMan.searchGenre(gen[j]);
                    loadRecentSongContainer(GenreSongContainer, temp, 1);
                    GenreSongContainer.repaint();
                }

            });
            GenreMenu.add(temp);
            totalHeightItem += temp.getHeight();
            if (totalHeightItem > defaultHeightMenu) {
                GenreMenu.setPreferredSize(new Dimension(defaultWidthMenu, totalHeightItem));
            }
        }
    }

    private void loadGenreMenu(ResultSet rs) throws SQLException {
        //Component[] GenreMenuCom=GenreMenu.getComponents();
        //JPanel[] menulist = new JPanel[20];
        ArrayList<JPanel> menulist = new ArrayList<>();
        int width = GenreMenu.getWidth();
        int yaxis = 0;
        int defaultHeightMenu = GenreMenu.getHeight();
        int defaultWidthMenu = GenreMenu.getHeight();
        int totalHeightItem = 0;
        int i = -1;
        while (rs.next()) {
            i++;
            JPanel temp = new JPanel();
            temp.setSize(width, 30);
            temp.setLayout(null);
            JLabel label = new JLabel(rs.getString(genreCol));
            label.setSize(35, 14);
            label.setLocation(20, 8);
            label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            temp.add(label);
            temp.setBackground(GenreMenuColor);

            if (i == 0) {
                yaxis = 0;
            } else {
                yaxis += temp.getHeight();
            }
            temp.setLocation(0, yaxis);
            temp.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    temp.setBackground(cursorColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    temp.setBackground(GenreMenuColor);
                    GenreMenu.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    GenreSongContainer.removeAll();

                    loadSongContainer(GenreSongContainer);

                    GenreSongContainer.repaint();
                }

            });
            GenreMenu.add(temp);
            totalHeightItem += temp.getHeight();
            if (totalHeightItem > defaultHeightMenu) {
                GenreMenu.setPreferredSize(new Dimension(defaultWidthMenu, totalHeightItem));
            }
        }
    }

    private void loadSongContainer(JPanel container) {
        JPanel[] songItems = new JPanel[10];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();

        for (int i = 0; i < songItems.length; i++) {
            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);
            imageSongItem.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));
            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel artist = new JLabel("Artist " + i);
            artist.setLocation(10, 160);
            artist.setSize(130, 14);
            JLabel composer = new JLabel("Composer " + i);
            composer.setLocation(10, 180);
            composer.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }
            });

            songItem.add(songImageContainer);
            songItem.add(artist);
            songItem.add(composer);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;
            songItems[i] = songItem;
            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    private void loadRecentSongContainer(JPanel container, ArrayList<Song> list, int type) {
        container.removeAll();
        container.setPreferredSize(defaultRecentDimension);
        if (type == 1) {
            container.setPreferredSize(defaultGenreSongDimension);
        }

        if (list.isEmpty()) {
            return;
        }
        JPanel[] songItems = new JPanel[list.size()];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();

        for (int i = 0; i < list.size(); i++) {
            Song song = list.get(i);
            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            byte[] image = song.getImage();
            if (image == null) {
                image = defaultImageSong;
            }

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);
            imageSongItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));
            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel Songname = new JLabel(song.getSongName());
            Songname.setLocation(10, 160);
            Songname.setSize(130, 14);
            JLabel artist = new JLabel(song.getArtist());
            artist.setLocation(10, 180);
            artist.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (nowPlaying.isUsed()) {
                        killNowPlaying = true;
                    }
                    nowPlaying = new NowPlaying();
                    nowPlaying.add(song);
                    play();
                    resetShuffRepeat();
                }

            });

            songItem.add(songImageContainer);
            songItem.add(Songname);
            songItem.add(artist);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;
            songItems[i] = songItem;
            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
        container.repaint();
    }

    private void loadAlbumContainer(JPanel container, Set<String> Album) {
        container.removeAll();
        container.setPreferredSize(defaultAlbumDimension);

        if (Album.isEmpty()) {
            return;
        }
        String[] albums = (String[]) Album.toArray(new String[Album.size()]);
        JPanel[] songItems = new JPanel[albums.length];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();

        for (int i = 0; i < albums.length; i++) {
            ArrayList<Song> listSong = songMan.searchAlbum(albums[i]);

            int count = 0;
            Song temp;
            do {
                temp = listSong.get(count);
                if (temp.getImage() != null) {
                    break;
                }
                count++;
            } while (count < listSong.size());

            byte[] image;
            if (temp.getImage() == null) {
                image = defaultImageAlbum;
            } else {
                image = temp.getImage();
            }

            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);

            try {
                imageSongItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));

            } catch (Exception e) {
                System.out.println("catch exception");
            }

            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel labelalbum = new JLabel(temp.getAlbum());
            labelalbum.setLocation(10, 160);
            labelalbum.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            final int k = i;
            byte[] imageClone = image;
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    container.getParent().getParent().setVisible(false);

                    loadAlbumPage((Component) container.getParent().getParent(), albums[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestAlbum = new HashSet<>(setAlbum);
                    theRestAlbum.remove(albums[k]);
                    loadSuggestionAlbum(suggestion, theRestAlbum);
                    infoArtist.setVisible(true);

                }

            });

            songItem.add(songImageContainer);
            songItem.add(labelalbum);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;
            songItems[i] = songItem;
            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    private void loadArtistContainer(JPanel container, Set<String> Artist) {
        container.removeAll();
        container.setPreferredSize(defaultArtistDimension);

        if (Artist.isEmpty()) {
            return;
        }
        String[] artists = (String[]) Artist.toArray(new String[Artist.size()]);
        JPanel[] songItems = new JPanel[artists.length];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();

        for (int i = 0; i < artists.length; i++) {
            ArrayList<Song> listSong = songMan.searchArtist(artists[i]);
            if (listSong.isEmpty()) {
                continue;
            }

            int count = 0;
            Song temp = listSong.get(0);

            while (count < listSong.size()) {
                temp = listSong.get(count);
                if (temp.getImage() != null) {
                    break;
                }
                count++;
            }

            byte[] image;
            if (temp.getImage() == null) {
                image = defaultImageArtist;
            } else {
                image = temp.getImage();
            }

            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);

            imageSongItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));

            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel labelArtist = new JLabel(temp.getArtist());
            labelArtist.setLocation(10, 160);
            labelArtist.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            final int k = i;
            byte[] imageClone = image;
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    container.getParent().getParent().setVisible(false);

                    loadArtistPage((Component) container.getParent().getParent(), artists[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestArtistSet = new HashSet<>(setArtist);
                    theRestArtistSet.remove(artists[k]);
                    loadSuggestionArtist(suggestion, theRestArtistSet);
                    infoArtist.setVisible(true);

                }

            });

            songItem.add(songImageContainer);
            songItem.add(labelArtist);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;
            songItems[i] = songItem;
            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    private void loadPlayListContainer(JPanel container) {
        if (playlist.isEmpty()) {
            return;
        }

        container.removeAll();
        container.setPreferredSize(defaultArtistDimension);

        Set<String> playlistSet = playlist.getPlaylist();
        String[] arrplaylist = (String[]) playlistSet.toArray(new String[playlistSet.size()]);

        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();

        for (int i = 0; i < arrplaylist.length; i++) {
            ArrayList<Song> listSong = playlist.getSongInPlaylist(arrplaylist[i]);
            Song temp;
            byte[] image;
            if (listSong.isEmpty()) {
                image = defaultImageSong;
            } else {
                int count = 0;
                temp = listSong.get(0);

                while (count < listSong.size()) {
                    temp = listSong.get(count);
                    if (temp.getImage() != null) {
                        break;
                    }
                    count++;
                }

                if (temp.getImage() == null) {
                    image = defaultImageSong;
                } else {
                    image = temp.getImage();
                }
            }

            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);

            imageSongItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));

            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel labelArtist = new JLabel(arrplaylist[i]);
            labelArtist.setLocation(10, 160);
            labelArtist.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            final int k = i;
            byte[] imageClone = image;
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    container.getParent().getParent().setVisible(false);

                    loadPlaylistPage((Component) container.getParent().getParent(), arrplaylist[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestPlaylistSet = new HashSet<>(playlist.keySet());
                    theRestPlaylistSet.remove(arrplaylist[k]);
                    loadSuggestionPlaylist(suggestion, theRestPlaylistSet);
                    infoArtist.setVisible(true);

                }

            });

            songItem.add(songImageContainer);
            songItem.add(labelArtist);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;

            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    private void loadSongContainer(JPanel container, ResultSet rs) throws SQLException {

        JPanel[] songItems = new JPanel[10];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getParent().getParent().getHeight();
        int defaultWidthContainer = container.getParent().getParent().getWidth();
        System.out.println(defaultHeightContainer + "|" + defaultWidthContainer);
        while (rs.next()) {
            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);
            imageSongItem.setIcon(new ImageIcon(new ImageIcon(rs.getString(songImageCol)).getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));
            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel artist = new JLabel(rs.getString(artistNameCol));
            artist.setLocation(10, 160);
            artist.setSize(130, 14);
            JLabel composer = new JLabel(rs.getString(composerNameCol));
            composer.setLocation(10, 180);
            composer.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }
            });

            songItem.add(songImageContainer);
            songItem.add(artist);
            songItem.add(composer);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;

            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    // note clear other relative labelalbum panel
    private void loadArtistPage(Component backPanel) {
        artistInfoName.setText("tung mini");
        artistImage.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(artistImage.getWidth(), artistImage.getHeight(), Image.SCALE_SMOOTH)));
        artistDescription.setText("hello\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh");
        if (backArtist.getMouseListeners().length != 0) {
            backArtist.removeMouseListener(backArtist.getMouseListeners()[0]);
        }
        backArtist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                infoArtist.setVisible(false);
                backPanel.setVisible(true);
                artistInfoName.setText("");
                artistImage.setIcon(null);
                artistDescription.setText("");
                songList.removeAll();
                songList.setPreferredSize(new Dimension(620, 260));
                suggestion.removeAll();
                suggestion.setPreferredSize(new Dimension(290, 390));
                // note clear other relative labelalbum panel

            }

        });
    }

    private void loadArtistPage(Component backPanel, String artist, byte[] image) {
        artistInfoName.setText(artist);
        artistImage.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(artistImage.getWidth(), artistImage.getHeight(), Image.SCALE_SMOOTH)));
        artistDescription.setText("");
        if (backArtist.getMouseListeners().length != 0) {
            backArtist.removeMouseListener(backArtist.getMouseListeners()[0]);
        }
        backArtist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                infoArtist.setVisible(false);

                artistInfoName.setText("");
                artistImage.setIcon(null);
                artistDescription.setText("");
                songList.removeAll();
                songList.setPreferredSize(new Dimension(620, 260));
                suggestion.removeAll();
                suggestion.setPreferredSize(new Dimension(290, 390));
                // note clear other relative labelalbum panel
                setAllUnVisible();
                //loadArtistContainer(ArtistPanel, setArtist);
                scrollArtistPanel.setVisible(true);
                System.out.println(ArtistPanel.isVisible());

            }

        });
    }

    private void loadAlbumPage(Component backPanel, String album, byte[] image) {
        artistInfoName.setText(album);
        artistImage.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(artistImage.getWidth(), artistImage.getHeight(), Image.SCALE_SMOOTH)));
        artistDescription.setText("");
        if (backArtist.getMouseListeners().length != 0) {
            backArtist.removeMouseListener(backArtist.getMouseListeners()[0]);
        }
        backArtist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                infoArtist.setVisible(false);
                scrollAlbumPanel.setVisible(true);
                //loadAlbumContainer(albumPanel, setAlbum);

                artistInfoName.setText("");
                artistImage.setIcon(null);
                artistDescription.setText("");
                songList.removeAll();
                songList.setPreferredSize(new Dimension(620, 260));
                suggestion.removeAll();
                suggestion.setPreferredSize(new Dimension(290, 390));
                // note clear other relative labelalbum panel

            }

        });
    }

    private void loadPlaylistPage(Component backPanel, String playlist, byte[] image) {
        artistInfoName.setText(playlist);
        artistImage.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(artistImage.getWidth(), artistImage.getHeight(), Image.SCALE_SMOOTH)));
        artistDescription.setText("");
        if (backArtist.getMouseListeners().length != 0) {
            backArtist.removeMouseListener(backArtist.getMouseListeners()[0]);
        }
        backArtist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                infoArtist.setVisible(false);
                scrollPlaylistPanel.setVisible(true);
                loadPlayListContainer(playlistPanel);

                artistInfoName.setText("");
                artistImage.setIcon(null);
                artistDescription.setText("");
                songList.removeAll();
                songList.setPreferredSize(new Dimension(620, 260));
                suggestion.removeAll();
                suggestion.setPreferredSize(new Dimension(290, 390));
                // note clear other relative labelalbum panel

            }

        });
    }

    private void loadSuggestion(JPanel container) {
        JPanel[] Items = new JPanel[10];
        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < Items.length; i++) {
            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(290, 80);

            JLabel avatarItem = new JLabel();
            avatarItem.setSize(80, 80);
            avatarItem.setLocation(0, 0);
            avatarItem.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));

            JLabel nameItem = new JLabel();

            nameItem.setSize(200, 80);
            nameItem.setLocation(90, 0);
            nameItem.setText("suggestion " + i);
            Item.add(avatarItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(290, 80);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }
            });
            Item.setLocation(0, yaxis);
            yaxis += 80;
            totalHeightItem = yaxis;
            Items[i] = Item;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }
    }

    private void loadSuggestionArtist(JPanel container, Set<String> artists) {
        suggestion.removeAll();
        suggestion.setPreferredSize(defaultSuggestionDimension);

        String[] listArtist = artists.toArray(new String[artists.size()]);

        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < listArtist.length; i++) {
            ArrayList<Song> listSong = songMan.searchArtist(listArtist[i]);
            if (listSong.isEmpty()) {
                continue;
            }

            int count = 0;
            Song temp = listSong.get(0);

            while (count < listSong.size()) {
                temp = listSong.get(count);
                if (temp.getImage() != null) {
                    break;
                }
                count++;
            }

            byte[] image = temp.getImage();
            if (temp.getImage() == null) {
                image = defaultImageArtist;
            } else {
                image = temp.getImage();
            }

            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(290, 80);

            JLabel avatarItem = new JLabel();
            avatarItem.setSize(80, 80);
            avatarItem.setLocation(0, 0);
            avatarItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));

            JLabel nameItem = new JLabel();

            nameItem.setSize(200, 80);
            nameItem.setLocation(90, 0);
            nameItem.setText(temp.getArtist());
            Item.add(avatarItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(290, 80);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            int k = i;
            byte[] imageClone = image;
            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {

                    loadArtistPage(ArtistPanel, listArtist[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestArtistSet = new HashSet<>(setArtist);

                    theRestArtistSet.remove(listArtist[k]);
                    loadSuggestionArtist(suggestion, theRestArtistSet);
                    infoArtist.repaint();
                    infoArtist.setVisible(true);
                }

            });
            Item.setLocation(0, yaxis);
            yaxis += 80;
            totalHeightItem = yaxis;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }
    }

    private void loadSuggestionAlbum(JPanel container, Set<String> albums) {
        suggestion.removeAll();
        suggestion.setPreferredSize(defaultSuggestionDimension);

        String[] listAlbum = albums.toArray(new String[albums.size()]);

        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < listAlbum.length; i++) {
            ArrayList<Song> listSong = songMan.searchAlbum(listAlbum[i]);
            if (listSong.isEmpty()) {
                continue;
            }

            int count = 0;
            Song temp = listSong.get(0);

            while (count < listSong.size()) {
                temp = listSong.get(count);
                if (temp.getImage() != null) {
                    break;
                }
                count++;
            }

            byte[] image = temp.getImage();
            if (temp.getImage() == null) {
                image = defaultImageAlbum;
            } else {
                image = temp.getImage();
            }

            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(290, 80);

            JLabel avatarItem = new JLabel();
            avatarItem.setSize(80, 80);
            avatarItem.setLocation(0, 0);
            avatarItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));

            JLabel nameItem = new JLabel();

            nameItem.setSize(200, 80);
            nameItem.setLocation(90, 0);
            nameItem.setText(temp.getArtist());
            Item.add(avatarItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(290, 80);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            int k = i;
            byte[] imageClone = image;
            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {

                    loadAlbumPage(albumPanel, listAlbum[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestAlbumSet = new HashSet<>(setAlbum);

                    theRestAlbumSet.remove(listAlbum[k]);
                    loadSuggestionAlbum(suggestion, theRestAlbumSet);
                    infoArtist.repaint();
                    infoArtist.setVisible(true);
                }

            });
            Item.setLocation(0, yaxis);
            yaxis += 80;
            totalHeightItem = yaxis;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }
    }

    private void loadSuggestionPlaylist(JPanel container, Set<String> playlist) {

        suggestion.removeAll();
        suggestion.setPreferredSize(defaultSuggestionDimension);
        if (playlist.isEmpty()) {
            return;
        }

        String[] listAlbum = playlist.toArray(new String[playlist.size()]);
        if (listAlbum.length == 0) {
            return;
        }

        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < listAlbum.length; i++) {
            ArrayList<Song> listSong = this.playlist.getSongInPlaylist(listAlbum[i]);
            if (listSong.isEmpty()) {
                continue;
            }

            int count = 0;
            Song temp = listSong.get(0);

            while (count < listSong.size()) {
                temp = listSong.get(count);
                if (temp.getImage() != null) {
                    break;
                }
                count++;
            }

            byte[] image = temp.getImage();
            if (temp.getImage() == null) {
                image = defaultImageAlbum;
            } else {
                image = temp.getImage();
            }

            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(290, 80);

            JLabel avatarItem = new JLabel();
            avatarItem.setSize(80, 80);
            avatarItem.setLocation(0, 0);
            avatarItem.setIcon(new ImageIcon(new ImageIcon(image).getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));

            JLabel nameItem = new JLabel();

            nameItem.setSize(200, 80);
            nameItem.setLocation(90, 0);
            nameItem.setText(temp.getArtist());
            Item.add(avatarItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(290, 80);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            int k = i;
            byte[] imageClone = image;
            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {

                    loadPlaylistPage(playlistPanel, listAlbum[k], imageClone);
                    loadSongList(songList, listSong);
                    Set<String> theRestPlaylistSet = new HashSet<>(setAlbum);

                    theRestPlaylistSet.remove(listAlbum[k]);
                    loadSuggestionPlaylist(suggestion, theRestPlaylistSet);
                    infoArtist.repaint();
                    infoArtist.setVisible(true);
                }

            });
            Item.setLocation(0, yaxis);
            yaxis += 80;
            totalHeightItem = yaxis;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }
    }

    private void loadSuggestion(JPanel container, ArrayList<Song> list) {
        container.removeAll();
        container.setPreferredSize(defaultSuggestionDimension);

        JPanel[] Items = new JPanel[10];
        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < Items.length; i++) {
            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(290, 80);

            JLabel avatarItem = new JLabel();
            avatarItem.setSize(80, 80);
            avatarItem.setLocation(0, 0);
            avatarItem.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(avatarItem.getWidth(), avatarItem.getHeight(), Image.SCALE_SMOOTH)));

            JLabel nameItem = new JLabel();

            nameItem.setSize(200, 80);
            nameItem.setLocation(90, 0);
            nameItem.setText("suggestion " + i);
            Item.add(avatarItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(290, 80);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }
            });
            Item.setLocation(0, yaxis);
            yaxis += 80;
            totalHeightItem = yaxis;
            Items[i] = Item;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }
    }

    private void loadPlayListSubMenu(JPanel container) {
        JPanel[] Items = new JPanel[10];
        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < Items.length; i++) {
            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(120, 30);

            JLabel iconItem = new JLabel();
            iconItem.setSize(30, 30);
            iconItem.setLocation(10, 0);
            iconItem.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));

            JLabel nameItem = new JLabel();

            nameItem.setSize(80, 14);
            nameItem.setLocation(10, 8);
            Item.add(iconItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(120, 30);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }
            });
            Item.setLocation(0, yaxis);
            yaxis += 30;
            totalHeightItem = yaxis;
            Items[i] = Item;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }

    }

    private void loadPlayListSubMenu(JPanel container, Song song) {
        container.removeAll();
        container.setPreferredSize(defaultPlayListSubMenuDimension);

        while (addToNowPlaying.getMouseListeners().length != 0) {
            addToNowPlaying.removeMouseListener(addToNowPlaying.getMouseListeners()[0]);
        }
        addToNowPlaying.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                playListSubMenuContainer.setVisible(false);
                transparentPanel.setVisible(false);
                nowPlaying.add(song);
                if (!nowPlaying.isUsed()) {
                    play();
                }

            }

        });

        Set<String> temp = playlist.getPlaylist();
        String[] playlistNames = (String[]) temp.toArray(new String[temp.size()]);
        JPanel[] Items = new JPanel[10];
        int yaxis = 0;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int dedaultWeightContainer = container.getWidth();
        for (int i = 0; i < playlistNames.length; i++) {
            JPanel Item = new JPanel(null);
            Item.setBackground(Color.white);
            Item.setSize(120, 30);

            JLabel iconItem = new JLabel();
            iconItem.setSize(30, 30);
            iconItem.setLocation(10, 0);
            iconItem.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));

            JLabel nameItem = new JLabel();

            nameItem.setSize(80, 14);
            nameItem.setLocation(40, 8);
            nameItem.setText(playlistNames[i]);
            Item.add(iconItem);
            Item.add(nameItem);

            JPanel layer = new JPanel(null);
            layer.setSize(120, 30);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            Item.add(layer);
            layer.setVisible(false);

            int k = i;
            Item.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    container.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    playlist.addSong(song, playlistNames[k]);
                    playListSubMenuContainer.setVisible(false);
                    transparentPanel.setVisible(false);
                }

            });
            Item.setLocation(0, yaxis);
            yaxis += 30;
            totalHeightItem = yaxis;
            Items[i] = Item;

            Item.setVisible(true);
            container.add(Item);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(dedaultWeightContainer, totalHeightItem));
            }

        }

    }

    private void loadArtistNAlbumContainer(JPanel container, int tab) {
        String s;
        if (tab == 0) {
            s = "artist";
        } else {
            s = "album";
        }
        JPanel[] songItems = new JPanel[10];
        int xaxis = 26;
        int yaxis = 26;
        int totalHeightItem = yaxis;
        int defaultHeightContainer = container.getHeight();
        int defaultWidthContainer = container.getWidth();
        for (int i = 0; i < songItems.length; i++) {
            JPanel songItem = new JPanel(null);
            songItem.setSize(150, 200);

            JPanel songImageContainer = new JPanel(null);
            songImageContainer.setSize(150, 150);
            songImageContainer.setLocation(0, 0);

            JLabel imageSongItem = new JLabel();
            imageSongItem.setSize(150, 150);
            imageSongItem.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(imageSongItem.getWidth(), imageSongItem.getHeight(), Image.SCALE_SMOOTH)));
            imageSongItem.setLocation(0, 0);

            JPanel layer = new JPanel(null);
            layer.setSize(150, 150);
            layer.setBackground(hoverImage);
            layer.setLocation(0, 0);

            JLabel playIcon = new JLabel();
            playIcon.setSize(50, 50);
            playIcon.setIcon(new ImageIcon("src/Images/icons8_Circled_Play_50px.png"));
            playIcon.setLocation(50, 50);
            playIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            layer.add(playIcon);

            JLabel artist = new JLabel("Artist " + i + s);
            artist.setLocation(10, 160);
            artist.setSize(130, 14);
            JLabel composer = new JLabel("Composer " + i);
            composer.setLocation(10, 180);
            composer.setSize(130, 14);

            songImageContainer.add(layer);
            layer.setVisible(false);
            songImageContainer.add(imageSongItem);
            songImageContainer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    layer.setVisible(true);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    layer.setVisible(false);
                    songImageContainer.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    container.getParent().getParent().setVisible(false);

                    loadArtistPage((Component) container.getParent().getParent());
                    loadSongList(songList);
                    loadSuggestion(suggestion);
                    infoArtist.setVisible(true);

                }

            });

            songItem.add(songImageContainer);
            songItem.add(artist);
            songItem.add(composer);

            if (xaxis + 176 > container.getWidth()) {
                xaxis = 26;
                yaxis += 220;
            } else {

            }

            totalHeightItem = yaxis + 200;
            songItems[i] = songItem;
            songItem.setLocation(xaxis, yaxis);

            songItem.setVisible(true);
            container.add(songItem);
            if (totalHeightItem > defaultHeightContainer) {
                container.setPreferredSize(new Dimension(defaultWidthContainer, totalHeightItem));
            }
            xaxis += 176;

        }
    }

    private void loadSongList(JPanel container) {
        JPanel[] SongItems = new JPanel[10];
        int xaxist = 0;
        int yaxist = 0;
        int defaultHeight = container.getHeight();
        int defaultWidth = container.getWidth();
        int totalHeight = 0;
        for (int i = 0; i < SongItems.length; i++) {
            JPanel SongItem = new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);
            JLabel infoSong = new JLabel();
            infoSong.setSize(500, 14);
            infoSong.setLocation(20, 8);
            infoSong.setText("song " + i + " - Artist" + i);
            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            JLabel iconDown = new JLabel();
            iconDown.setSize(30, 30);
            iconDown.setLocation(520, 0);
            iconDown.setIcon(new ImageIcon("src/Images/icons8_Download_from_the_Cloud_20px.png"));

            JLabel iconPlay = new JLabel();
            iconPlay.setSize(30, 30);
            iconPlay.setLocation(550, 0);
            iconPlay.setIcon(new ImageIcon("src/Images/icons8_Play_20px.png"));

            JLabel iconAdd = new JLabel();
            iconAdd.setSize(30, 30);
            iconAdd.setLocation(580, 0);
            iconAdd.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));
            iconAdd.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int x = iconAdd.getX();
                    int y = iconAdd.getParent().getY() + 40;
                    System.out.println("x, y" + x + "|" + y);
                    playListSubMenuContainer.setLocation(x, y);
                    loadPlayListSubMenu(playListSubMenu);
                    transparentPanel.setVisible(true);
                    playListSubMenuContainer.setVisible(true);
                    System.out.println("check");

                }

            });

            SongItem.add(infoSong);
            SongItem.add(iconDown);
            SongItem.add(iconPlay);
            SongItem.add(iconAdd);

            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }

            });

            totalHeight += 30;
            if (totalHeight > defaultHeight) {
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0, yaxist);
            container.add(SongItem);
            yaxist += 30;
        }
    }

    private void loadSongList(JPanel container, ArrayList<Song> list) {
        container.removeAll();
        container.setPreferredSize(defaultSongListDimension);

        while (PlayAllListOfSongButton.getMouseListeners().length != 0) {
            PlayAllListOfSongButton.removeMouseListener(PlayAllListOfSongButton.getMouseListeners()[0]);
        }
        PlayAllListOfSongButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (nowPlaying.isUsed()) {
                    killNowPlaying = true;
                }
                nowPlaying.setList(list);
                play();
                resetShuffRepeat();
            }

        });

        while (shuffAllSongButton.getMouseListeners().length != 0) {
            shuffAllSongButton.removeMouseListener(shuffAllSongButton.getMouseListeners()[0]);
        }
        shuffAllSongButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (nowPlaying.isUsed()) {
                    killNowPlaying = true;

                }
                nowPlaying.setList(list);
                nowPlaying.shuffer();
                play();
                resetShuffRepeat();
                shuffStatus = true;
                shuffPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Shuffle_25px_active.png"));

            }

        });

        JPanel[] SongItems = new JPanel[10];
        int xaxist = 0;
        int yaxist = 0;
        int defaultHeight = container.getHeight();
        int defaultWidth = container.getWidth();
        int totalHeight = 0;
        for (int i = 0; i < list.size(); i++) {
            Song song = list.get(i);

            JPanel SongItem = new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);
            JLabel infoSong = new JLabel();
            infoSong.setSize(500, 14);
            infoSong.setLocation(20, 8);
            infoSong.setText(song.getSongName() + " - " + song.getArtist());
            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            JLabel iconPlay = new JLabel();
            iconPlay.setSize(30, 30);
            iconPlay.setLocation(550, 0);
            iconPlay.setIcon(new ImageIcon("src/Images/icons8_Play_20px.png"));
            iconPlay.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (nowPlaying.isUsed()) {
                        killNowPlaying = true;
                    }
                    nowPlaying = new NowPlaying();
                    nowPlaying.add(song);
                    play();
                    resetShuffRepeat();
                }

            });

            JLabel iconAdd = new JLabel();
            iconAdd.setSize(30, 30);
            iconAdd.setLocation(580, 0);
            iconAdd.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));
            iconAdd.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int x = iconAdd.getX();
                    int y = iconAdd.getParent().getY() + 40;
                    System.out.println("x, y" + x + "|" + y);
                    playListSubMenuContainer.setLocation(x, y);
                    loadPlayListSubMenu(playListSubMenu, song);
                    transparentPanel.setVisible(true);
                    playListSubMenuContainer.setVisible(true);
                    System.out.println("check");

                }

            });

            SongItem.add(infoSong);
            SongItem.add(iconPlay);
            SongItem.add(iconAdd);

            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }

            });

            totalHeight += 30;
            if (totalHeight > defaultHeight) {
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0, yaxist);
            container.add(SongItem);
            yaxist += 30;
        }
    }

    private void loadSongListSearch(JPanel container) {
        JPanel[] SongItems = new JPanel[10];
        int xaxist = 0;
        int yaxist = 0;
        int defaultHeight = container.getHeight();
        int defaultWidth = container.getWidth();
        int totalHeight = 0;
        for (int i = 0; i < SongItems.length; i++) {
            JPanel SongItem = new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);

            JLabel infoSong = new JLabel();
            infoSong.setSize(200, 14);
            infoSong.setLocation(20, 8);
            infoSong.setText("song " + i);

            JLabel infoArtist = new JLabel();
            infoArtist.setSize(200, 14);
            infoArtist.setLocation(220, 8);
            infoArtist.setText("Artist " + i);

            JLabel infoAlbum = new JLabel();
            infoAlbum.setSize(200, 14);
            infoAlbum.setLocation(420, 8);
            infoAlbum.setText("Album " + i);

            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            JLabel iconDown = new JLabel();
            iconDown.setSize(30, 30);
            iconDown.setLocation(800, 0);
            iconDown.setIcon(new ImageIcon("src/Images/icons8_Download_from_the_Cloud_20px.png"));

            JLabel iconPlay = new JLabel();
            iconPlay.setSize(30, 30);
            iconPlay.setLocation(830, 0);
            iconPlay.setIcon(new ImageIcon("src/Images/icons8_Play_20px.png"));

            JLabel iconAdd = new JLabel();
            iconAdd.setSize(30, 30);
            iconAdd.setLocation(860, 0);
            iconAdd.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));
            iconAdd.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int x = iconAdd.getX() - 40;
                    int y = iconAdd.getParent().getY();
                    System.out.println("x, y" + x + "|" + y);
                    playListSubMenuContainer.setLocation(x, y);
                    loadPlayListSubMenu(playListSubMenu);
                    transparentPanel.setVisible(true);
                    playListSubMenuContainer.setVisible(true);
                    System.out.println("check");
                }

            });

            SongItem.add(infoSong);
            SongItem.add(infoArtist);
            SongItem.add(infoAlbum);
            SongItem.add(iconDown);
            SongItem.add(iconPlay);
            SongItem.add(iconAdd);

            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }

            });

            totalHeight += 30;
            if (totalHeight > defaultHeight) {
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0, yaxist);
            container.add(SongItem);
            yaxist += 30;
        }
    }

    private void loadSongListSearch(JPanel container, ArrayList<Song> list) {
        container.removeAll();
        container.setPreferredSize(defaultSongListSearchDimension);

        while (playSearchResult.getMouseListeners().length != 0) {
            playSearchResult.removeMouseListener(playSearchResult.getMouseListeners()[0]);
        }
        playSearchResult.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (nowPlaying.isUsed()) {
                    killNowPlaying = true;
                }
                nowPlaying.setList(list);
                play();
                resetShuffRepeat();
            }

        });

        while (shuffSearchResult.getMouseListeners().length != 0) {
            shuffSearchResult.removeMouseListener(shuffSearchResult.getMouseListeners()[0]);
        }
        shuffSearchResult.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (nowPlaying.isUsed()) {
                    killNowPlaying = true;

                }
                nowPlaying.setList(list);
                nowPlaying.shuffer();
                play();
                resetShuffRepeat();
                shuffStatus = true;
                shuffPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Shuffle_25px_active.png"));

            }

        });

        JPanel[] SongItems = new JPanel[10];
        int xaxist = 0;
        int yaxist = 0;
        int defaultHeight = container.getHeight();
        int defaultWidth = container.getWidth();
        int totalHeight = 0;
        for (int i = 0; i < list.size(); i++) {
            Song song = list.get(i);
            JPanel SongItem = new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);

            JLabel infoSong = new JLabel();
            infoSong.setSize(200, 14);
            infoSong.setLocation(20, 8);
            infoSong.setText(song.getSongName());

            JLabel infoArtist = new JLabel();
            infoArtist.setSize(200, 14);
            infoArtist.setLocation(220, 8);
            infoArtist.setText(song.getArtist());

            JLabel infoAlbum = new JLabel();
            infoAlbum.setSize(200, 14);
            infoAlbum.setLocation(420, 8);
            infoAlbum.setText(song.getAlbum());

            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            JLabel iconPlay = new JLabel();
            iconPlay.setSize(30, 30);
            iconPlay.setLocation(830, 0);
            iconPlay.setIcon(new ImageIcon("src/Images/icons8_Play_20px.png"));
            iconPlay.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (nowPlaying.isUsed()) {
                        killNowPlaying = true;
                    }
                    nowPlaying = new NowPlaying();
                    nowPlaying.add(song);
                    play();
                    resetShuffRepeat();
                }

            });

            JLabel iconAdd = new JLabel();
            iconAdd.setSize(30, 30);
            iconAdd.setLocation(860, 0);
            iconAdd.setIcon(new ImageIcon("src/Images/icons8_Plus_Math_20px.png"));
            iconAdd.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int x = iconAdd.getX() - 40;
                    int y = iconAdd.getParent().getY();
                    System.out.println("x, y" + x + "|" + y);
                    playListSubMenuContainer.setLocation(x, y);
                    loadPlayListSubMenu(playListSubMenu, song);
                    transparentPanel.setVisible(true);
                    playListSubMenuContainer.setVisible(true);
                    System.out.println("check");
                }

            });

            SongItem.add(infoSong);
            SongItem.add(infoArtist);
            SongItem.add(infoAlbum);
            SongItem.add(iconPlay);
            SongItem.add(iconAdd);

            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }

            });

            totalHeight += 30;
            if (totalHeight > defaultHeight) {
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0, yaxist);
            container.add(SongItem);
            yaxist += 30;
        }
        container.repaint();
    }

    private void loadNowPlayingList(JPanel container, ArrayList<Song> list) {
        container.removeAll();
        container.setPreferredSize(defaultNowPlayingListDimension);

        int xaxist = 0;
        int yaxist = 0;
        int defaultHeight = container.getHeight();
        int defaultWidth = container.getWidth();
        int totalHeight = 0;
        for (int i = 0; i < list.size(); i++) {
            Song song = list.get(i);

            JPanel SongItem = new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);
            JLabel infoSong = new JLabel();
            infoSong.setSize(160, 14);
            infoSong.setLocation(0, 8);
            infoSong.setText(song.getSongName());
            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            int j = i;
            infoSong.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    nowPlaying.setItterator(j);
                    if (!nowPlaying.isUsed()) {
                        play();
                    } else {
                        skip = true;
                    }
                }
            });

            SongItem.add(infoSong);

            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (nowPlaying.isUsed()) {
                        killNowPlaying = true;
                    }
                    nowPlaying = new NowPlaying();
                    nowPlaying.add(song);
                    play();
                    resetShuffRepeat();
                }

            });

            totalHeight += 30;
            if (totalHeight > defaultHeight) {
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0, yaxist);
            container.add(SongItem);
            yaxist += 30;
        }
    }

    private void setMouseClickSidebarItems() {

        for (int i = 1; i < extendTab.size() + 1; i++) {
            JPanel temp = (JPanel) sidebar.getComponent(i);
            final int j = i;
            temp.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (j != markTab) {
                        for (Component x : extendTab) {
                            x.setVisible(false);
                        }
                        extendTab.get(j - 1).setVisible(true);
                        /*if(j==3&&infoArtist.isVisible()){
                            extendTab.get(2).setVisible(false);
                        }*/
                        //infoArtist.setVisible(false);
                        markTab = j;
                        System.out.println("press");
                    }
                }

            });
        }
    }

    private void initSideBar() {
        RecentExtend.setVisible(false);
        RecentExtend.setBackground(hover);
        GenreExtend.setVisible(false);
        GenreExtend.setBackground(hover);
        ArtistExtend.setVisible(false);
        ArtistExtend.setBackground(hover);
        PlayListExtend.setVisible(false);
        PlayListExtend.setBackground(hover);
        AlbumsExtend.setVisible(false);
        AlbumsExtend.setBackground(hover);
        NewPlayListExtend.setVisible(false);
        NewPlayListExtend.setBackground(hover);

    }

    private void initSeek() {
        seekCursor.setBackground(cursorColor);
        seekCursor.setBounds(0, 0, 0, 0);
        /*new Thread() {
            @Override
            public void run() {

                while (SkipCursor.getWidth() < skipControl.getWidth()) {
                    if (player.length != 0) {
                        SkipCursor.setSize((int) (SkipCursor.getWidth() + skipControl.getWidth() / player.length), 60);
                        System.out.println("thread increase width");
                        try {
                            sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }.start();*/
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainpanel = new javax.swing.JPanel();
        transparentPanel = new javax.swing.JPanel();
        playListSubMenuContainer = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        addToNowPlaying = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        newPlayListSubMenu = new javax.swing.JLabel();
        scrollPlayListSubMenu = new javax.swing.JScrollPane();
        playListSubMenu = new javax.swing.JPanel();
        createPlaylistContainer = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        nameNewPlaylist = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        createPlaylistButton = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        cancelCreatePlaylist = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        Loading = new javax.swing.JLabel();
        accountInforContainer = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        changeAvatarButton = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        logoutButton = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        addFoldersContainer = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        addFolderButton = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        scrollBarFoldersList = new javax.swing.JScrollPane();
        foldersListContainer = new javax.swing.JPanel();
        nowPlayingContainer = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        scrollNowPlaying = new javax.swing.JScrollPane();
        nowPlayingList = new javax.swing.JPanel();
        PlayListExtend = new javax.swing.JPanel();
        jLabelExtendInfoDiscover6 = new javax.swing.JLabel();
        NewPlayListExtend = new javax.swing.JPanel();
        jLabelExtendInfoDiscover1 = new javax.swing.JLabel();
        sidebar = new javax.swing.JPanel();
        logo = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Recent = new javax.swing.JPanel();
        jLabelRecent = new javax.swing.JLabel();
        Genre = new javax.swing.JPanel();
        jLabelGenre = new javax.swing.JLabel();
        Artist = new javax.swing.JPanel();
        jLabelArtist = new javax.swing.JLabel();
        Albums = new javax.swing.JPanel();
        jLabelAlbums = new javax.swing.JLabel();
        Playlist = new javax.swing.JPanel();
        jLabelPlaylist = new javax.swing.JLabel();
        CreatePlaylist = new javax.swing.JPanel();
        jLabelCreatePlaylist = new javax.swing.JLabel();
        Avatar = new javax.swing.JPanel();
        ArtistExtend = new javax.swing.JPanel();
        jLabelExtendInfoDiscover3 = new javax.swing.JLabel();
        PlayerContainer = new javax.swing.JPanel();
        PlayerControler = new javax.swing.JPanel();
        PlayerControlerPrev = new javax.swing.JPanel();
        prevSongPlayerController = new javax.swing.JLabel();
        playButtonPlayerController = new javax.swing.JLabel();
        nextSongPlayerController = new javax.swing.JLabel();
        nowPlayingController = new javax.swing.JLabel();
        seekControl = new javax.swing.JPanel();
        seekCursor = new javax.swing.JPanel();
        ArtistName_SkipControl = new javax.swing.JLabel();
        SongName_SkipControl = new javax.swing.JLabel();
        ControlOrderPlayer = new javax.swing.JPanel();
        TimeDisplayContainer = new javax.swing.JPanel();
        CurrentTimeOfSkipCursor = new javax.swing.JLabel();
        LengthOfSong = new javax.swing.JLabel();
        TimeDisplayContainer1 = new javax.swing.JPanel();
        CurrentTimeOfSkipCursor1 = new javax.swing.JLabel();
        volumeSlider = new javax.swing.JPanel();
        volumeCursor = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        shuffPlayerControl = new javax.swing.JLabel();
        repeatPlayerControl = new javax.swing.JLabel();
        GenreExtend = new javax.swing.JPanel();
        jLabelExtendInfoDiscover2 = new javax.swing.JLabel();
        RecentExtend = new javax.swing.JPanel();
        jLabelExtendInfoRecent = new javax.swing.JLabel();
        AlbumsExtend = new javax.swing.JPanel();
        jLabelExtendInfoDiscover5 = new javax.swing.JLabel();
        Header = new javax.swing.JPanel();
        Control_Setting_Exit = new javax.swing.JPanel();
        ExitButton = new javax.swing.JLabel();
        SettingButton = new javax.swing.JLabel();
        reloadButton = new javax.swing.JLabel();
        Search = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        SearchBarContainer = new javax.swing.JPanel();
        searchBar = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        moveControl = new javax.swing.JLabel();
        scrollRecentPanel = new javax.swing.JScrollPane();
        RecentPanel = new javax.swing.JPanel();
        GenrePanel = new javax.swing.JPanel();
        scrollGenreMenuPanel = new javax.swing.JScrollPane();
        GenreMenu = new javax.swing.JPanel();
        scrollGenrePanel = new javax.swing.JScrollPane();
        GenreSongContainer = new javax.swing.JPanel();
        scrollArtistPanel = new javax.swing.JScrollPane();
        ArtistPanel = new javax.swing.JPanel();
        scrollAlbumPanel = new javax.swing.JScrollPane();
        albumPanel = new javax.swing.JPanel();
        scrollPlaylistPanel = new javax.swing.JScrollPane();
        playlistPanel = new javax.swing.JPanel();
        infoArtist = new javax.swing.JPanel();
        artistDetail = new javax.swing.JPanel();
        artistImagePanel = new javax.swing.JPanel();
        artistImage = new javax.swing.JLabel();
        scrollDescriptionArtist = new javax.swing.JScrollPane();
        artistDescription = new javax.swing.JTextArea();
        generalInfo = new javax.swing.JPanel();
        artistInfoName = new javax.swing.JLabel();
        backArtist = new javax.swing.JLabel();
        relativeArtist = new javax.swing.JPanel();
        headerSideBar = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        scrollOtherAritst = new javax.swing.JScrollPane();
        suggestion = new javax.swing.JPanel();
        songListContainer = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        shuffAllSongButton = new javax.swing.JLabel();
        PlayAllListOfSongButton = new javax.swing.JLabel();
        scrollSongList = new javax.swing.JScrollPane();
        songList = new javax.swing.JPanel();
        searchResultPanel = new javax.swing.JPanel();
        searchContainer = new javax.swing.JPanel();
        scrollSongListSearchResult = new javax.swing.JScrollPane();
        songListSearchResult = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        playSearchResult = new javax.swing.JLabel();
        shuffSearchResult = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mainpanel.setBackground(new java.awt.Color(255, 255, 255));
        mainpanel.setForeground(new java.awt.Color(255, 255, 255));
        mainpanel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mainpanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        transparentPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        transparentPanel.setLayout(null);

        playListSubMenuContainer.setBackground(new java.awt.Color(255, 255, 255));
        playListSubMenuContainer.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        addToNowPlaying.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        addToNowPlaying.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addToNowPlaying.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Audio_Wave_20px_3.png"))); // NOI18N
        addToNowPlaying.setText("Now Playing");
        addToNowPlaying.setMaximumSize(new java.awt.Dimension(123, 25));
        addToNowPlaying.setMinimumSize(new java.awt.Dimension(123, 25));
        jPanel2.add(addToNowPlaying);
        addToNowPlaying.setBounds(0, 0, 120, 20);
        jPanel2.add(jSeparator1);
        jSeparator1.setBounds(0, 20, 120, 10);

        playListSubMenuContainer.add(jPanel2);
        jPanel2.setBounds(0, 0, 120, 30);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        newPlayListSubMenu.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        newPlayListSubMenu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        newPlayListSubMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_New_20px.png"))); // NOI18N
        newPlayListSubMenu.setText("New Playlist");
        newPlayListSubMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                newPlayListSubMenuMouseClicked(evt);
            }
        });
        jPanel3.add(newPlayListSubMenu);
        newPlayListSubMenu.setBounds(0, 0, 120, 20);

        playListSubMenuContainer.add(jPanel3);
        jPanel3.setBounds(0, 30, 120, 20);

        scrollPlayListSubMenu.setBackground(new java.awt.Color(255, 255, 255));
        scrollPlayListSubMenu.setBorder(null);

        playListSubMenu.setBackground(new java.awt.Color(255, 255, 255));
        playListSubMenu.setLayout(null);
        scrollPlayListSubMenu.setViewportView(playListSubMenu);

        playListSubMenuContainer.add(scrollPlayListSubMenu);
        scrollPlayListSubMenu.setBounds(0, 50, 120, 120);

        transparentPanel.add(playListSubMenuContainer);
        playListSubMenuContainer.setBounds(150, 0, 120, 170);

        createPlaylistContainer.setBackground(new java.awt.Color(255, 255, 255));
        createPlaylistContainer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                createPlaylistContainerMouseClicked(evt);
            }
        });
        createPlaylistContainer.setLayout(null);

        jPanel1.setBackground(new java.awt.Color(76, 182, 203));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setPreferredSize(new java.awt.Dimension(150, 150));
        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jPanel1MouseReleased(evt);
            }
        });
        jPanel1.setLayout(null);

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Playlist_70px.png"))); // NOI18N
        jLabel15.setPreferredSize(new java.awt.Dimension(150, 150));
        jPanel1.add(jLabel15);
        jLabel15.setBounds(0, 0, 110, 100);

        createPlaylistContainer.add(jPanel1);
        jPanel1.setBounds(95, 10, 110, 100);

        jPanel6.setBackground(new java.awt.Color(76, 182, 203));
        jPanel6.setPreferredSize(new java.awt.Dimension(290, 50));
        jPanel6.setLayout(null);

        jLabel16.setBackground(new java.awt.Color(24, 31, 36));
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Edit_30px.png"))); // NOI18N
        jPanel6.add(jLabel16);
        jLabel16.setBounds(120, 0, 30, 30);

        nameNewPlaylist.setBackground(new java.awt.Color(24, 31, 36));
        nameNewPlaylist.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nameNewPlaylist.setForeground(new java.awt.Color(255, 255, 255));
        nameNewPlaylist.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nameNewPlaylist.setText("Name this playlist");
        nameNewPlaylist.setPreferredSize(new java.awt.Dimension(146, 30));
        nameNewPlaylist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nameNewPlaylistMouseClicked(evt);
            }
        });
        nameNewPlaylist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameNewPlaylistActionPerformed(evt);
            }
        });
        jPanel6.add(nameNewPlaylist);
        nameNewPlaylist.setBounds(0, 0, 120, 30);

        createPlaylistContainer.add(jPanel6);
        jPanel6.setBounds(75, 120, 150, 30);

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Created by you");
        createPlaylistContainer.add(jLabel17);
        jLabel17.setBounds(110, 160, 80, 14);
        createPlaylistContainer.add(jSeparator2);
        jSeparator2.setBounds(100, 180, 100, 2);

        createPlaylistButton.setBackground(new java.awt.Color(76, 182, 203));
        createPlaylistButton.setForeground(new java.awt.Color(255, 255, 255));
        createPlaylistButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                createPlaylistButtonMouseClicked(evt);
            }
        });
        createPlaylistButton.setLayout(null);

        jLabel18.setBackground(new java.awt.Color(153, 153, 153));
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Create playlist");
        jLabel18.setMaximumSize(new java.awt.Dimension(70, 14));
        jLabel18.setMinimumSize(new java.awt.Dimension(70, 14));
        jLabel18.setPreferredSize(new java.awt.Dimension(70, 14));
        createPlaylistButton.add(jLabel18);
        jLabel18.setBounds(0, 0, 130, 30);

        createPlaylistContainer.add(createPlaylistButton);
        createPlaylistButton.setBounds(85, 200, 130, 30);

        cancelCreatePlaylist.setBackground(new java.awt.Color(76, 182, 203));
        cancelCreatePlaylist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelCreatePlaylistMouseClicked(evt);
            }
        });
        cancelCreatePlaylist.setLayout(null);

        jLabel19.setBackground(new java.awt.Color(204, 204, 255));
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Cancel");
        cancelCreatePlaylist.add(jLabel19);
        jLabel19.setBounds(0, 0, 90, 30);

        createPlaylistContainer.add(cancelCreatePlaylist);
        cancelCreatePlaylist.setBounds(105, 250, 90, 30);

        transparentPanel.add(createPlaylistContainer);
        createPlaylistContainer.setBounds(335, 130, 300, 300);

        Loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Ripple-2.3s-200px (2).gif"))); // NOI18N
        transparentPanel.add(Loading);
        Loading.setBounds(385, 180, 200, 200);

        accountInforContainer.setLayout(null);

        jPanel7.setBackground(new java.awt.Color(24, 31, 36));
        jPanel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel7MouseExited(evt);
            }
        });
        jPanel7.setLayout(null);

        jLabel20.setBackground(new java.awt.Color(102, 102, 102));
        jLabel20.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_User_Menu_Male_25px.png"))); // NOI18N
        jPanel7.add(jLabel20);
        jLabel20.setBounds(0, 0, 40, 40);

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("username");
        jPanel7.add(jLabel24);
        jLabel24.setBounds(50, 0, 70, 40);

        accountInforContainer.add(jPanel7);
        jPanel7.setBounds(0, 0, 120, 40);

        changeAvatarButton.setBackground(new java.awt.Color(24, 31, 36));
        changeAvatarButton.setForeground(new java.awt.Color(255, 255, 255));
        changeAvatarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                changeAvatarButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                changeAvatarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                changeAvatarButtonMouseExited(evt);
            }
        });
        changeAvatarButton.setLayout(null);

        jLabel21.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Change Avatar");
        changeAvatarButton.add(jLabel21);
        jLabel21.setBounds(40, 0, 80, 40);

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Full_Image_25px.png"))); // NOI18N
        changeAvatarButton.add(jLabel25);
        jLabel25.setBounds(8, 0, 30, 40);

        accountInforContainer.add(changeAvatarButton);
        changeAvatarButton.setBounds(0, 40, 120, 40);

        logoutButton.setBackground(new java.awt.Color(24, 31, 36));
        logoutButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                logoutButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                logoutButtonMouseExited(evt);
            }
        });
        logoutButton.setLayout(null);

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Sign_Out_25px.png"))); // NOI18N
        logoutButton.add(jLabel22);
        jLabel22.setBounds(0, 0, 40, 40);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Logout");
        logoutButton.add(jLabel23);
        jLabel23.setBounds(50, 0, 70, 40);

        accountInforContainer.add(logoutButton);
        logoutButton.setBounds(0, 80, 120, 40);

        transparentPanel.add(accountInforContainer);
        accountInforContainer.setBounds(0, 380, 120, 120);

        addFoldersContainer.setBackground(new java.awt.Color(255, 255, 255));
        addFoldersContainer.setLayout(null);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setForeground(new java.awt.Color(255, 255, 255));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jPanel5MouseReleased(evt);
            }
        });
        jPanel5.setLayout(null);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Build your collection from your local music files");
        jPanel5.add(jLabel2);
        jLabel2.setBounds(10, 10, 300, 20);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel3.setText("Right now, we're watching these folders:");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(20, 40, 310, 20);

        addFoldersContainer.add(jPanel5);
        jPanel5.setBounds(0, 0, 320, 90);

        addFolderButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addFolderButtonMouseClicked(evt);
            }
        });
        addFolderButton.setLayout(null);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Plus_Math_50px.png"))); // NOI18N
        addFolderButton.add(jLabel9);
        jLabel9.setBounds(65, 10, 50, 50);

        addFoldersContainer.add(addFolderButton);
        addFolderButton.setBounds(70, 90, 180, 70);

        scrollBarFoldersList.setBorder(null);

        foldersListContainer.setBackground(new java.awt.Color(255, 255, 255));
        foldersListContainer.setPreferredSize(new java.awt.Dimension(320, 400));
        foldersListContainer.setLayout(null);
        scrollBarFoldersList.setViewportView(foldersListContainer);

        addFoldersContainer.add(scrollBarFoldersList);
        scrollBarFoldersList.setBounds(0, 200, 320, 200);

        transparentPanel.add(addFoldersContainer);
        addFoldersContainer.setBounds(325, 80, 320, 400);

        nowPlayingContainer.setBackground(new java.awt.Color(255, 255, 255));
        nowPlayingContainer.setLayout(null);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setLayout(null);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Audio_Wave_20px_3.png"))); // NOI18N
        jLabel4.setText("Now Playing");
        jPanel11.add(jLabel4);
        jLabel4.setBounds(30, 0, 100, 20);

        nowPlayingContainer.add(jPanel11);
        jPanel11.setBounds(0, 0, 160, 20);

        scrollNowPlaying.setBorder(null);

        nowPlayingList.setBackground(new java.awt.Color(255, 255, 255));
        nowPlayingList.setLayout(null);
        scrollNowPlaying.setViewportView(nowPlayingList);

        nowPlayingContainer.add(scrollNowPlaying);
        scrollNowPlaying.setBounds(0, 20, 160, 130);

        transparentPanel.add(nowPlayingContainer);
        nowPlayingContainer.setBounds(60, 350, 160, 150);

        mainpanel.add(transparentPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 970, 560));

        PlayListExtend.setBackground(new java.awt.Color(24, 31, 36));
        PlayListExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        PlayListExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                PlayListExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                PlayListExtendMouseExited(evt);
            }
        });
        PlayListExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoDiscover6.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoDiscover6.setText("PlayList");
        PlayListExtend.add(jLabelExtendInfoDiscover6, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 10, 50, 20));

        mainpanel.add(PlayListExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, 90, 40));

        NewPlayListExtend.setBackground(new java.awt.Color(24, 31, 36));
        NewPlayListExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        NewPlayListExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                NewPlayListExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                NewPlayListExtendMouseExited(evt);
            }
        });
        NewPlayListExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoDiscover1.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoDiscover1.setText("New PlayList");
        NewPlayListExtend.add(jLabelExtendInfoDiscover1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 80, 20));

        mainpanel.add(NewPlayListExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 460, 90, 40));

        sidebar.setBackground(new java.awt.Color(24, 31, 36));
        sidebar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        sidebar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        logo.setBackground(new java.awt.Color(76, 182, 203));
        logo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Headphones_32px.png"))); // NOI18N
        logo.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 15, 30, 30));

        sidebar.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 60));

        Recent.setBackground(new java.awt.Color(24, 31, 36));
        Recent.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Recent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RecentMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RecentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                RecentMouseExited(evt);
            }
        });
        Recent.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelRecent.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Astronaut_Helmet_32px.png"))); // NOI18N
        Recent.add(jLabelRecent, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 5, 30, 30));

        sidebar.add(Recent, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 60, 40));

        Genre.setBackground(new java.awt.Color(24, 31, 36));
        Genre.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Genre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                GenreMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GenreMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                GenreMouseExited(evt);
            }
        });
        Genre.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelGenre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Deezer_32px.png"))); // NOI18N
        Genre.add(jLabelGenre, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 10, 30, 20));

        sidebar.add(Genre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 60, 40));

        Artist.setBackground(new java.awt.Color(24, 31, 36));
        Artist.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Artist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ArtistMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ArtistMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ArtistMouseExited(evt);
            }
        });
        Artist.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelArtist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_DJ_32px.png"))); // NOI18N
        Artist.add(jLabelArtist, new org.netbeans.lib.awtextra.AbsoluteConstraints(14, 5, 30, 30));

        sidebar.add(Artist, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 60, 40));

        Albums.setBackground(new java.awt.Color(24, 31, 36));
        Albums.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Albums.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AlbumsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AlbumsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AlbumsMouseExited(evt);
            }
        });
        Albums.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelAlbums.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Music_Album_32px.png"))); // NOI18N
        Albums.add(jLabelAlbums, new org.netbeans.lib.awtextra.AbsoluteConstraints(14, 10, 30, 20));

        sidebar.add(Albums, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 60, 40));

        Playlist.setBackground(new java.awt.Color(24, 31, 36));
        Playlist.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Playlist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PlaylistMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                PlaylistMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                PlaylistMouseExited(evt);
            }
        });
        Playlist.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelPlaylist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Lounge_Music_Playlist_32px.png"))); // NOI18N
        Playlist.add(jLabelPlaylist, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 10, 30, 20));

        sidebar.add(Playlist, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, 60, 40));

        CreatePlaylist.setBackground(new java.awt.Color(24, 31, 36));
        CreatePlaylist.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        CreatePlaylist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CreatePlaylistMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CreatePlaylistMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CreatePlaylistMouseExited(evt);
            }
        });
        CreatePlaylist.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelCreatePlaylist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Add_32px.png"))); // NOI18N
        CreatePlaylist.add(jLabelCreatePlaylist, new org.netbeans.lib.awtextra.AbsoluteConstraints(14, 5, 30, 30));

        sidebar.add(CreatePlaylist, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 460, 60, 40));

        Avatar.setBackground(new java.awt.Color(76, 182, 203));
        Avatar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AvatarMouseClicked(evt);
            }
        });
        Avatar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        sidebar.add(Avatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 60, 60));

        mainpanel.add(sidebar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 560));

        ArtistExtend.setBackground(new java.awt.Color(24, 31, 36));
        ArtistExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ArtistExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ArtistExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ArtistExtendMouseExited(evt);
            }
        });
        ArtistExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoDiscover3.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoDiscover3.setText("Artist");
        ArtistExtend.add(jLabelExtendInfoDiscover3, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 10, 50, 20));

        mainpanel.add(ArtistExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 140, 90, 40));

        PlayerContainer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PlayerControler.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PlayerControlerPrev.setBackground(new java.awt.Color(90, 106, 122));
        PlayerControlerPrev.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PlayerControlerPrev.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        prevSongPlayerController.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Rewind_25px.png"))); // NOI18N
        prevSongPlayerController.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        prevSongPlayerController.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prevSongPlayerControllerMouseClicked(evt);
            }
        });
        PlayerControlerPrev.add(prevSongPlayerController, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 15, 30, 30));

        playButtonPlayerController.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Play_32px.png"))); // NOI18N
        playButtonPlayerController.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        playButtonPlayerController.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                playButtonPlayerControllerMouseReleased(evt);
            }
        });
        PlayerControlerPrev.add(playButtonPlayerController, new org.netbeans.lib.awtextra.AbsoluteConstraints(45, 15, 30, 30));

        nextSongPlayerController.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Fast_Forward_25px.png"))); // NOI18N
        nextSongPlayerController.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        nextSongPlayerController.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nextSongPlayerControllerMouseClicked(evt);
            }
        });
        PlayerControlerPrev.add(nextSongPlayerController, new org.netbeans.lib.awtextra.AbsoluteConstraints(82, 15, 30, 30));

        nowPlayingController.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Lounge_Music_Playlist_25px.png"))); // NOI18N
        nowPlayingController.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        nowPlayingController.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nowPlayingControllerMouseClicked(evt);
            }
        });
        PlayerControlerPrev.add(nowPlayingController, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 15, 30, 30));

        PlayerControler.add(PlayerControlerPrev, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 60));

        PlayerContainer.add(PlayerControler, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 60));

        seekControl.setBackground(new java.awt.Color(77, 93, 110));
        seekControl.setMinimumSize(new java.awt.Dimension(0, 60));
        seekControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                seekControlMouseReleased(evt);
            }
        });
        seekControl.setLayout(null);

        seekCursor.setBackground(new java.awt.Color(76, 182, 203));
        seekCursor.setPreferredSize(new java.awt.Dimension(10, 60));
        seekCursor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        seekControl.add(seekCursor);
        seekCursor.setBounds(0, 0, 0, 60);

        ArtistName_SkipControl.setForeground(new java.awt.Color(255, 255, 255));
        ArtistName_SkipControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        seekControl.add(ArtistName_SkipControl);
        ArtistName_SkipControl.setBounds(80, 30, 300, 20);

        SongName_SkipControl.setForeground(new java.awt.Color(255, 255, 255));
        SongName_SkipControl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        seekControl.add(SongName_SkipControl);
        SongName_SkipControl.setBounds(80, 10, 300, 20);

        PlayerContainer.add(seekControl, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 450, 60));

        ControlOrderPlayer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TimeDisplayContainer.setBackground(new java.awt.Color(90, 106, 122));
        TimeDisplayContainer.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TimeDisplayContainer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        CurrentTimeOfSkipCursor.setForeground(new java.awt.Color(255, 255, 255));
        CurrentTimeOfSkipCursor.setText("00:00");
        TimeDisplayContainer.add(CurrentTimeOfSkipCursor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 23, 40, 14));

        LengthOfSong.setForeground(new java.awt.Color(255, 255, 255));
        LengthOfSong.setText("00:00");
        TimeDisplayContainer.add(LengthOfSong, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 23, 40, 14));

        ControlOrderPlayer.add(TimeDisplayContainer, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, 60));

        TimeDisplayContainer1.setBackground(new java.awt.Color(90, 106, 122));
        TimeDisplayContainer1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TimeDisplayContainer1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        CurrentTimeOfSkipCursor1.setForeground(new java.awt.Color(255, 255, 255));
        CurrentTimeOfSkipCursor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Speaker_25px.png"))); // NOI18N
        CurrentTimeOfSkipCursor1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        TimeDisplayContainer1.add(CurrentTimeOfSkipCursor1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 15, 30, 30));

        volumeSlider.setBackground(new java.awt.Color(77, 93, 110));
        volumeSlider.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volumeSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                volumeSliderMouseReleased(evt);
            }
        });
        volumeSlider.setLayout(null);

        volumeCursor.setBackground(new java.awt.Color(76, 182, 203));
        volumeSlider.add(volumeCursor);
        volumeCursor.setBounds(0, 0, 40, 10);

        TimeDisplayContainer1.add(volumeSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 25, 80, 10));

        ControlOrderPlayer.add(TimeDisplayContainer1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 120, 60));

        jPanel10.setBackground(new java.awt.Color(90, 106, 122));
        jPanel10.setLayout(null);

        shuffPlayerControl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Shuffle_25px.png"))); // NOI18N
        shuffPlayerControl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        shuffPlayerControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shuffPlayerControlMouseClicked(evt);
            }
        });
        jPanel10.add(shuffPlayerControl);
        shuffPlayerControl.setBounds(10, 0, 35, 60);

        repeatPlayerControl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Repeat_25px.png"))); // NOI18N
        repeatPlayerControl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        repeatPlayerControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                repeatPlayerControlMouseClicked(evt);
            }
        });
        jPanel10.add(repeatPlayerControl);
        repeatPlayerControl.setBounds(45, 0, 35, 60);

        ControlOrderPlayer.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 0, 80, 60));

        PlayerContainer.add(ControlOrderPlayer, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 0, 300, 60));

        mainpanel.add(PlayerContainer, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 500, 910, 60));

        GenreExtend.setBackground(new java.awt.Color(24, 31, 36));
        GenreExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        GenreExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GenreExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                GenreExtendMouseExited(evt);
            }
        });
        GenreExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoDiscover2.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoDiscover2.setText("Genre");
        GenreExtend.add(jLabelExtendInfoDiscover2, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 10, 50, 20));

        mainpanel.add(GenreExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 100, 90, 40));

        RecentExtend.setBackground(new java.awt.Color(24, 31, 36));
        RecentExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        RecentExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RecentExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                RecentExtendMouseExited(evt);
            }
        });
        RecentExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoRecent.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoRecent.setText("Recent");
        RecentExtend.add(jLabelExtendInfoRecent, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 10, 50, 20));

        mainpanel.add(RecentExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 90, 40));

        AlbumsExtend.setBackground(new java.awt.Color(24, 31, 36));
        AlbumsExtend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AlbumsExtend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AlbumsExtendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AlbumsExtendMouseExited(evt);
            }
        });
        AlbumsExtend.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelExtendInfoDiscover5.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExtendInfoDiscover5.setText("Albums");
        AlbumsExtend.add(jLabelExtendInfoDiscover5, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 10, 50, 20));

        mainpanel.add(AlbumsExtend, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 180, 90, 40));

        Header.setBackground(new java.awt.Color(255, 255, 255));
        Header.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Control_Setting_Exit.setBackground(new java.awt.Color(255, 255, 255));
        Control_Setting_Exit.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ExitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Cancel_20px.png"))); // NOI18N
        ExitButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ExitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                ExitButtonMouseReleased(evt);
            }
        });
        Control_Setting_Exit.add(ExitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 0, 20, 20));

        SettingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Settings_20px.png"))); // NOI18N
        SettingButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        SettingButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SettingButtonMouseClicked(evt);
            }
        });
        Control_Setting_Exit.add(SettingButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 20));

        reloadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Synchronize_20px_1.png"))); // NOI18N
        reloadButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        reloadButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reloadButtonMouseClicked(evt);
            }
        });
        Control_Setting_Exit.add(reloadButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 0, 20, 20));

        Header.add(Control_Setting_Exit, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 0, 160, 30));

        Search.setBackground(new java.awt.Color(255, 255, 255));
        Search.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SearchMouseClicked(evt);
            }
        });
        Search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Search_32px.png"))); // NOI18N
        Search.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 15, 30, 30));

        Header.add(Search, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 60));

        SearchBarContainer.setBackground(new java.awt.Color(255, 255, 255));
        SearchBarContainer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        searchBar.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(120, 129, 136)));
        SearchBarContainer.add(searchBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 330, -1));

        Header.add(SearchBarContainer, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 0, 350, 60));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        Header.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 0, 60, 60));

        mainpanel.add(Header, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 0, 910, 60));

        moveControl.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                moveControlMouseDragged(evt);
            }
        });
        moveControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                moveControlMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                moveControlMouseReleased(evt);
            }
        });
        mainpanel.add(moveControl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 850, 15));

        scrollRecentPanel.setBorder(null);

        RecentPanel.setBackground(new java.awt.Color(255, 255, 255));
        RecentPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        RecentPanel.setPreferredSize(new java.awt.Dimension(910, 660));
        RecentPanel.setLayout(null);
        scrollRecentPanel.setViewportView(RecentPanel);

        mainpanel.add(scrollRecentPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        GenrePanel.setBackground(new java.awt.Color(255, 255, 255));
        GenrePanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        GenrePanel.setPreferredSize(new java.awt.Dimension(910, 440));
        GenrePanel.setLayout(null);

        scrollGenreMenuPanel.setBorder(null);

        GenreMenu.setBackground(new java.awt.Color(204, 204, 204));
        GenreMenu.setLayout(null);
        scrollGenreMenuPanel.setViewportView(GenreMenu);

        GenrePanel.add(scrollGenreMenuPanel);
        scrollGenreMenuPanel.setBounds(0, 0, 160, 440);

        scrollGenrePanel.setBorder(null);

        GenreSongContainer.setBackground(new java.awt.Color(255, 255, 255));
        GenreSongContainer.setPreferredSize(new java.awt.Dimension(750, 440));
        GenreSongContainer.setLayout(null);
        scrollGenrePanel.setViewportView(GenreSongContainer);

        GenrePanel.add(scrollGenrePanel);
        scrollGenrePanel.setBounds(160, 0, 750, 440);

        mainpanel.add(GenrePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        scrollArtistPanel.setBorder(null);

        ArtistPanel.setBackground(new java.awt.Color(255, 255, 255));
        ArtistPanel.setLayout(null);
        scrollArtistPanel.setViewportView(ArtistPanel);

        mainpanel.add(scrollArtistPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        scrollAlbumPanel.setBorder(null);

        albumPanel.setBackground(new java.awt.Color(255, 255, 255));
        albumPanel.setLayout(null);
        scrollAlbumPanel.setViewportView(albumPanel);

        mainpanel.add(scrollAlbumPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        scrollPlaylistPanel.setBorder(null);

        playlistPanel.setBackground(new java.awt.Color(255, 255, 255));
        playlistPanel.setPreferredSize(new java.awt.Dimension(910, 440));
        playlistPanel.setLayout(null);
        scrollPlaylistPanel.setViewportView(playlistPanel);

        mainpanel.add(scrollPlaylistPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        infoArtist.setBackground(new java.awt.Color(255, 255, 255));
        infoArtist.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        infoArtist.setLayout(null);

        artistDetail.setBackground(new java.awt.Color(153, 153, 153));
        artistDetail.setLayout(null);

        artistImagePanel.setBackground(new java.awt.Color(255, 255, 255));
        artistImagePanel.setLayout(null);
        artistImagePanel.add(artistImage);
        artistImage.setBounds(0, 0, 150, 150);

        artistDetail.add(artistImagePanel);
        artistImagePanel.setBounds(0, 0, 150, 150);

        scrollDescriptionArtist.setBorder(null);

        artistDescription.setEditable(false);
        artistDescription.setColumns(20);
        artistDescription.setLineWrap(true);
        artistDescription.setRows(5);
        scrollDescriptionArtist.setViewportView(artistDescription);

        artistDetail.add(scrollDescriptionArtist);
        scrollDescriptionArtist.setBounds(150, 50, 470, 100);

        generalInfo.setBackground(new java.awt.Color(255, 255, 255));
        generalInfo.setLayout(null);

        artistInfoName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        artistInfoName.setText("Artist");
        generalInfo.add(artistInfoName);
        artistInfoName.setBounds(10, 10, 250, 30);

        backArtist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_Arrow_30px_1.png"))); // NOI18N
        generalInfo.add(backArtist);
        backArtist.setBounds(420, 10, 30, 30);

        artistDetail.add(generalInfo);
        generalInfo.setBounds(150, 0, 470, 50);

        infoArtist.add(artistDetail);
        artistDetail.setBounds(0, 0, 620, 150);

        relativeArtist.setBackground(new java.awt.Color(255, 255, 255));
        relativeArtist.setLayout(null);

        headerSideBar.setBackground(new java.awt.Color(255, 255, 255));
        headerSideBar.setLayout(null);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("Suggestion");
        headerSideBar.add(jLabel13);
        jLabel13.setBounds(30, 14, 230, 22);

        relativeArtist.add(headerSideBar);
        headerSideBar.setBounds(0, 0, 290, 50);

        scrollOtherAritst.setBorder(null);

        suggestion.setBackground(new java.awt.Color(255, 255, 255));
        suggestion.setPreferredSize(new java.awt.Dimension(290, 390));
        suggestion.setLayout(null);
        scrollOtherAritst.setViewportView(suggestion);

        relativeArtist.add(scrollOtherAritst);
        scrollOtherAritst.setBounds(0, 50, 290, 390);

        infoArtist.add(relativeArtist);
        relativeArtist.setBounds(620, 0, 290, 440);

        songListContainer.setBackground(new java.awt.Color(255, 255, 255));
        songListContainer.setLayout(null);

        jPanel9.setBackground(new java.awt.Color(24, 31, 36));
        jPanel9.setLayout(null);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Songs");
        jPanel9.add(jLabel14);
        jLabel14.setBounds(20, 0, 90, 30);

        shuffAllSongButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Shuffle_25px.png"))); // NOI18N
        jPanel9.add(shuffAllSongButton);
        shuffAllSongButton.setBounds(590, 0, 30, 30);

        PlayAllListOfSongButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Play_32px.png"))); // NOI18N
        jPanel9.add(PlayAllListOfSongButton);
        PlayAllListOfSongButton.setBounds(550, 0, 30, 30);

        songListContainer.add(jPanel9);
        jPanel9.setBounds(0, 0, 620, 30);

        scrollSongList.setBorder(null);

        songList.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout songListLayout = new javax.swing.GroupLayout(songList);
        songList.setLayout(songListLayout);
        songListLayout.setHorizontalGroup(
            songListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 620, Short.MAX_VALUE)
        );
        songListLayout.setVerticalGroup(
            songListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 260, Short.MAX_VALUE)
        );

        scrollSongList.setViewportView(songList);

        songListContainer.add(scrollSongList);
        scrollSongList.setBounds(0, 30, 620, 260);

        infoArtist.add(songListContainer);
        songListContainer.setBounds(0, 150, 620, 290);

        mainpanel.add(infoArtist, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        searchResultPanel.setLayout(null);

        searchContainer.setLayout(null);

        scrollSongListSearchResult.setBorder(null);

        songListSearchResult.setBackground(new java.awt.Color(255, 255, 255));
        songListSearchResult.setPreferredSize(new java.awt.Dimension(910, 380));

        javax.swing.GroupLayout songListSearchResultLayout = new javax.swing.GroupLayout(songListSearchResult);
        songListSearchResult.setLayout(songListSearchResultLayout);
        songListSearchResultLayout.setHorizontalGroup(
            songListSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 910, Short.MAX_VALUE)
        );
        songListSearchResultLayout.setVerticalGroup(
            songListSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 380, Short.MAX_VALUE)
        );

        scrollSongListSearchResult.setViewportView(songListSearchResult);

        searchContainer.add(scrollSongListSearchResult);
        scrollSongListSearchResult.setBounds(0, 60, 910, 380);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(null);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel26.setText("Search Result");
        jPanel8.add(jLabel26);
        jLabel26.setBounds(20, 15, 106, 30);

        playSearchResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Play_32px.png"))); // NOI18N
        jPanel8.add(playSearchResult);
        playSearchResult.setBounds(790, 14, 32, 32);

        shuffSearchResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Shuffle_32px.png"))); // NOI18N
        jPanel8.add(shuffSearchResult);
        shuffSearchResult.setBounds(850, 14, 32, 32);

        searchContainer.add(jPanel8);
        jPanel8.setBounds(0, 0, 910, 60);

        searchResultPanel.add(searchContainer);
        searchContainer.setBounds(0, 0, 910, 440);

        mainpanel.add(searchResultPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 910, 440));

        getContentPane().add(mainpanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 970, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RecentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RecentMouseEntered
        Recent.setBackground(hover);
        Recent.setBorder(borderLeft);
        RecentExtend.setVisible(true);
    }//GEN-LAST:event_RecentMouseEntered

    private void RecentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RecentMouseExited
        Recent.setBackground(sidebar_color);
        Recent.setBorder(null);
        RecentExtend.setVisible(false);
    }//GEN-LAST:event_RecentMouseExited

    private void GenreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GenreMouseEntered
        Genre.setBackground(hover);
        Genre.setBorder(borderLeft);
        GenreExtend.setVisible(true);
    }//GEN-LAST:event_GenreMouseEntered

    private void GenreMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GenreMouseExited
        Genre.setBackground(sidebar_color);
        Genre.setBorder(null);
        GenreExtend.setVisible(false);
    }//GEN-LAST:event_GenreMouseExited

    private void ArtistMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ArtistMouseEntered
        Artist.setBackground(hover);
        Artist.setBorder(borderLeft);
        ArtistExtend.setVisible(true);
    }//GEN-LAST:event_ArtistMouseEntered

    private void ArtistMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ArtistMouseExited
        Artist.setBackground(sidebar_color);
        Artist.setBorder(null);
        ArtistExtend.setVisible(false);
    }//GEN-LAST:event_ArtistMouseExited

    private void PlaylistMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PlaylistMouseEntered
        Playlist.setBackground(hover);
        Playlist.setBorder(borderLeft);
        PlayListExtend.setVisible(true);
    }//GEN-LAST:event_PlaylistMouseEntered

    private void PlaylistMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PlaylistMouseExited
        Playlist.setBackground(sidebar_color);
        Playlist.setBorder(null);
        PlayListExtend.setVisible(false);
    }//GEN-LAST:event_PlaylistMouseExited

    private void AlbumsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlbumsMouseEntered
        Albums.setBackground(hover);
        Albums.setBorder(borderLeft);
        AlbumsExtend.setVisible(true);
    }//GEN-LAST:event_AlbumsMouseEntered

    private void AlbumsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlbumsMouseExited
        Albums.setBackground(sidebar_color);
        Albums.setBorder(null);
        AlbumsExtend.setVisible(false);
    }//GEN-LAST:event_AlbumsMouseExited

    private void CreatePlaylistMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreatePlaylistMouseEntered
        CreatePlaylist.setBackground(hover);
        CreatePlaylist.setBorder(borderLeft);
        NewPlayListExtend.setVisible(true);
    }//GEN-LAST:event_CreatePlaylistMouseEntered

    private void CreatePlaylistMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreatePlaylistMouseExited
        CreatePlaylist.setBackground(sidebar_color);
        CreatePlaylist.setBorder(null);
        NewPlayListExtend.setVisible(false);
    }//GEN-LAST:event_CreatePlaylistMouseExited

    private void RecentExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RecentExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_RecentExtendMouseEntered

    private void RecentExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RecentExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_RecentExtendMouseExited

    private void NewPlayListExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_NewPlayListExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_NewPlayListExtendMouseEntered

    private void NewPlayListExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_NewPlayListExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_NewPlayListExtendMouseExited

    private void GenreExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GenreExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_GenreExtendMouseEntered

    private void GenreExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GenreExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_GenreExtendMouseExited

    private void ArtistExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ArtistExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_ArtistExtendMouseEntered

    private void ArtistExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ArtistExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_ArtistExtendMouseExited

    private void AlbumsExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlbumsExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_AlbumsExtendMouseEntered

    private void AlbumsExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlbumsExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_AlbumsExtendMouseExited

    private void PlayListExtendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PlayListExtendMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_PlayListExtendMouseEntered

    private void PlayListExtendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PlayListExtendMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_PlayListExtendMouseExited

    private void seekControlMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_seekControlMouseReleased

        int x = evt.getX();

        int time = player.getDuration() * x / seekControl.getWidth();

        player.seek(time);
    }//GEN-LAST:event_seekControlMouseReleased

    private void ExitButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ExitButtonMouseReleased
        try {
            saveConfigFile();
        } catch (IOException ex) {
            Logger.getLogger(Final_PRJ.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    }//GEN-LAST:event_ExitButtonMouseReleased

    private void moveControlMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_moveControlMouseDragged
        int x = evt.getXOnScreen() - xMouse;
        int y = evt.getYOnScreen() - yMouse;
        this.setLocation(x, y);
    }//GEN-LAST:event_moveControlMouseDragged

    private void moveControlMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_moveControlMousePressed
        xMouse = evt.getX();
        yMouse = evt.getY();
        this.setOpacity((float) 0.4);
    }//GEN-LAST:event_moveControlMousePressed

    private void moveControlMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_moveControlMouseReleased
        this.setOpacity((float) 1);
    }//GEN-LAST:event_moveControlMouseReleased

    private void playButtonPlayerControllerMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_playButtonPlayerControllerMouseReleased
        if (nowPlaying.isEmpty()) {
            return;
        }
        if (pause) {
            player.resume();
            playButtonPlayerController.setIcon(new ImageIcon("src/Images/icons8_Pause_32px.png"));
            pause = false;
        } else {
            player.pause();
            pause = true;
            playButtonPlayerController.setIcon(new ImageIcon("src/Images/icons8_Play_32px.png"));
        }
    }//GEN-LAST:event_playButtonPlayerControllerMouseReleased

    private void volumeSliderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_volumeSliderMouseReleased
        int x = evt.getX();
        volumeCursor.setBounds(0, 0, x, 10);
        player.setVolume(0.4f + ((float) x * 0.6f / volumeSlider.getWidth()));
        volumeSlider.repaint();

    }//GEN-LAST:event_volumeSliderMouseReleased

    private void RecentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RecentMouseClicked
        setAllUnVisible();
        scrollRecentPanel.setVisible(true);
        loadRecentSongContainer(RecentPanel, recent.getList(), 0);
    }//GEN-LAST:event_RecentMouseClicked

    private void GenreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GenreMouseClicked
        setAllUnVisible();
        GenrePanel.setVisible(true);
        /*loadGenreMenu(songMan.listAllGenre());
        loadRecentSongContainer(GenreSongContainer, songMan.listAllSong());*/
    }//GEN-LAST:event_GenreMouseClicked

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged

    }//GEN-LAST:event_jPanel1MouseDragged

    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed

    }//GEN-LAST:event_jPanel1MousePressed

    private void jPanel1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseReleased
        // TODO add your handling code here:
        setOpacity((float) 1.0);
    }//GEN-LAST:event_jPanel1MouseReleased

    private void nameNewPlaylistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nameNewPlaylistMouseClicked
        // TODO add your handling code here:
        searchBar.setText("");
    }//GEN-LAST:event_nameNewPlaylistMouseClicked

    private void nameNewPlaylistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameNewPlaylistActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameNewPlaylistActionPerformed

    private void CreatePlaylistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreatePlaylistMouseClicked
        transparentPanel.setVisible(true);
        createPlaylistContainer.setVisible(true);
    }//GEN-LAST:event_CreatePlaylistMouseClicked

    private void AvatarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AvatarMouseClicked
        transparentPanel.setVisible(true);
        accountInforContainer.setVisible(true);
    }//GEN-LAST:event_AvatarMouseClicked

    private void changeAvatarButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeAvatarButtonMouseClicked

    }//GEN-LAST:event_changeAvatarButtonMouseClicked

    private void jPanel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel7MouseClicked

    private void logoutButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutButtonMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_logoutButtonMouseClicked

    private void jPanel7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseEntered
        jPanel7.setBackground(hover);
    }//GEN-LAST:event_jPanel7MouseEntered

    private void jPanel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseExited
        jPanel7.setBackground(sidebar_color);
    }//GEN-LAST:event_jPanel7MouseExited

    private void changeAvatarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeAvatarButtonMouseEntered
        changeAvatarButton.setBackground(hover);
    }//GEN-LAST:event_changeAvatarButtonMouseEntered

    private void changeAvatarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeAvatarButtonMouseExited
        changeAvatarButton.setBackground(sidebar_color);
    }//GEN-LAST:event_changeAvatarButtonMouseExited

    private void logoutButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutButtonMouseEntered
        logoutButton.setBackground(hover);
    }//GEN-LAST:event_logoutButtonMouseEntered

    private void logoutButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutButtonMouseExited
        logoutButton.setBackground(sidebar_color);
    }//GEN-LAST:event_logoutButtonMouseExited

    private void createPlaylistContainerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_createPlaylistContainerMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_createPlaylistContainerMouseClicked

    private void SearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SearchMouseClicked
        String keyWord = searchBar.getText().trim();
        searchBar.setText("");
        if (keyWord.isEmpty()) {
            return;
        }
        setAllUnVisible();

        loadSongListSearch(songListSearchResult, songMan.searchAll(keyWord));
        searchResultPanel.setVisible(true);
    }//GEN-LAST:event_SearchMouseClicked

    private void SettingButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SettingButtonMouseClicked
        String[] Folders = pathsFolder.getListFolders();
        foldersListContainer.removeAll();
        foldersListContainer.setPreferredSize(defaultFolderListDimension);
        for (String path : Folders) {
            addFolders(foldersListContainer, path);
        }
        addFoldersContainer.setVisible(true);
        transparentPanel.setVisible(true);
    }//GEN-LAST:event_SettingButtonMouseClicked

    private void addFolderButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addFolderButtonMouseClicked
        JFileChooser fc = new JFileChooser();
        fc.setAcceptAllFileFilterUsed(true);
        fc.setMultiSelectionEnabled(true);
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File[] files = fc.getSelectedFiles();

            for (File f : files) {
                addFolders(foldersListContainer, f.getPath());
                System.out.println(f.getPath());
                pathsFolder.add(f.getPath());
                songMan.extract(f.getPath());
            }

        }
        setAlbum = songMan.listAllAlbum();
        setArtist = songMan.listAllArtist();
        loadGenreMenu(songMan.listAllGenre());
        loadRecentSongContainer(GenreSongContainer, songMan.listAllSong(), 1);
        loadArtistContainer(ArtistPanel, setArtist);
        loadAlbumContainer(albumPanel, setAlbum);
    }//GEN-LAST:event_addFolderButtonMouseClicked

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged

    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed

    }//GEN-LAST:event_jPanel5MousePressed

    private void jPanel5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseReleased
        // TODO add your handling code here:
        setOpacity((float) 1.0);
    }//GEN-LAST:event_jPanel5MouseReleased

    private void ArtistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ArtistMouseClicked
        setAllUnVisible();
        scrollArtistPanel.setVisible(true);
        //loadArtistContainer(ArtistPanel, setArtist);
    }//GEN-LAST:event_ArtistMouseClicked

    private void AlbumsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AlbumsMouseClicked
        setAllUnVisible();
        scrollAlbumPanel.setVisible(true);
        //loadAlbumContainer(albumPanel, setAlbum);
    }//GEN-LAST:event_AlbumsMouseClicked

    private void PlaylistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PlaylistMouseClicked
        setAllUnVisible();
        scrollPlaylistPanel.setVisible(true);
        loadPlayListContainer(playlistPanel);

    }//GEN-LAST:event_PlaylistMouseClicked

    private void createPlaylistButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_createPlaylistButtonMouseClicked
        String name = nameNewPlaylist.getText().trim();
        if (name.isEmpty()) {
            return;
        }
        playlist.addPlaylist(name);
        transparentPanel.setVisible(false);
        createPlaylistContainer.setVisible(false);
    }//GEN-LAST:event_createPlaylistButtonMouseClicked

    private void nextSongPlayerControllerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nextSongPlayerControllerMouseClicked
        if (nowPlaying.isEmpty() || !nowPlaying.isUsed()) {
            return;
        }
        next = true;

    }//GEN-LAST:event_nextSongPlayerControllerMouseClicked

    private void prevSongPlayerControllerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prevSongPlayerControllerMouseClicked
        if (nowPlaying.isEmpty() || !nowPlaying.isUsed()) {
            return;
        }
        prev = true;
    }//GEN-LAST:event_prevSongPlayerControllerMouseClicked

    private void shuffPlayerControlMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shuffPlayerControlMouseClicked
        if (shuffStatus) {
            shuffStatus = false;
            shuffPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Shuffle_25px.png"));
        } else {
            shuffStatus = true;
            nowPlaying.shuffer();
            nowPlaying.setItterator(0);
            shuffPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Shuffle_25px_active.png"));
        }

    }//GEN-LAST:event_shuffPlayerControlMouseClicked

    private void repeatPlayerControlMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_repeatPlayerControlMouseClicked
        if (repeatStatus) {
            repeatStatus = false;
            repeatPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Repeat_25px.png"));
        } else {
            repeatStatus = true;
            repeatPlayerControl.setIcon(new ImageIcon("src/Images/icons8_Repeat_One_25px.png"));

        }

    }//GEN-LAST:event_repeatPlayerControlMouseClicked

    private void newPlayListSubMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newPlayListSubMenuMouseClicked
        playListSubMenuContainer.setVisible(false);
        transparentPanel.setVisible(false);
        transparentPanel.setVisible(true);
        createPlaylistContainer.setVisible(true);
    }//GEN-LAST:event_newPlayListSubMenuMouseClicked

    private void cancelCreatePlaylistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelCreatePlaylistMouseClicked
        transparentPanel.setVisible(false);
        createPlaylistContainer.setVisible(false);
    }//GEN-LAST:event_cancelCreatePlaylistMouseClicked

    private void nowPlayingControllerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nowPlayingControllerMouseClicked
        transparentPanel.setVisible(true);
        loadNowPlayingList(nowPlayingList, nowPlaying.getList());
        nowPlayingContainer.setVisible(true);
    }//GEN-LAST:event_nowPlayingControllerMouseClicked

    private void reloadButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reloadButtonMouseClicked
        if (canReload) {
            loadSongMan = false;
            canReload=false;            
            loadAll();
        }
    }//GEN-LAST:event_reloadButtonMouseClicked

    private void clear(Graphics g) {
        super.paintComponents(g);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Final_PRJ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Final_PRJ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Final_PRJ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Final_PRJ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Final_PRJ().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Albums;
    private javax.swing.JPanel AlbumsExtend;
    private javax.swing.JPanel Artist;
    private javax.swing.JPanel ArtistExtend;
    private javax.swing.JLabel ArtistName_SkipControl;
    private javax.swing.JPanel ArtistPanel;
    private javax.swing.JPanel Avatar;
    private javax.swing.JPanel ControlOrderPlayer;
    private javax.swing.JPanel Control_Setting_Exit;
    private javax.swing.JPanel CreatePlaylist;
    private javax.swing.JLabel CurrentTimeOfSkipCursor;
    private javax.swing.JLabel CurrentTimeOfSkipCursor1;
    private javax.swing.JLabel ExitButton;
    private javax.swing.JPanel Genre;
    private javax.swing.JPanel GenreExtend;
    private javax.swing.JPanel GenreMenu;
    private javax.swing.JPanel GenrePanel;
    private javax.swing.JPanel GenreSongContainer;
    private javax.swing.JPanel Header;
    private javax.swing.JLabel LengthOfSong;
    private javax.swing.JLabel Loading;
    private javax.swing.JPanel NewPlayListExtend;
    private javax.swing.JLabel PlayAllListOfSongButton;
    private javax.swing.JPanel PlayListExtend;
    private javax.swing.JPanel PlayerContainer;
    private javax.swing.JPanel PlayerControler;
    private javax.swing.JPanel PlayerControlerPrev;
    private javax.swing.JPanel Playlist;
    private javax.swing.JPanel Recent;
    private javax.swing.JPanel RecentExtend;
    private javax.swing.JPanel RecentPanel;
    private javax.swing.JPanel Search;
    private javax.swing.JPanel SearchBarContainer;
    private javax.swing.JLabel SettingButton;
    private javax.swing.JLabel SongName_SkipControl;
    private javax.swing.JPanel TimeDisplayContainer;
    private javax.swing.JPanel TimeDisplayContainer1;
    private javax.swing.JPanel accountInforContainer;
    private javax.swing.JPanel addFolderButton;
    private javax.swing.JPanel addFoldersContainer;
    private javax.swing.JLabel addToNowPlaying;
    private javax.swing.JPanel albumPanel;
    private javax.swing.JTextArea artistDescription;
    private javax.swing.JPanel artistDetail;
    private javax.swing.JLabel artistImage;
    private javax.swing.JPanel artistImagePanel;
    private javax.swing.JLabel artistInfoName;
    private javax.swing.JLabel backArtist;
    private javax.swing.JPanel cancelCreatePlaylist;
    private javax.swing.JPanel changeAvatarButton;
    private javax.swing.JPanel createPlaylistButton;
    private javax.swing.JPanel createPlaylistContainer;
    private javax.swing.JPanel foldersListContainer;
    private javax.swing.JPanel generalInfo;
    private javax.swing.JPanel headerSideBar;
    private javax.swing.JPanel infoArtist;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAlbums;
    private javax.swing.JLabel jLabelArtist;
    private javax.swing.JLabel jLabelCreatePlaylist;
    private javax.swing.JLabel jLabelExtendInfoDiscover1;
    private javax.swing.JLabel jLabelExtendInfoDiscover2;
    private javax.swing.JLabel jLabelExtendInfoDiscover3;
    private javax.swing.JLabel jLabelExtendInfoDiscover5;
    private javax.swing.JLabel jLabelExtendInfoDiscover6;
    private javax.swing.JLabel jLabelExtendInfoRecent;
    private javax.swing.JLabel jLabelGenre;
    private javax.swing.JLabel jLabelPlaylist;
    private javax.swing.JLabel jLabelRecent;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPanel logo;
    private javax.swing.JPanel logoutButton;
    private javax.swing.JPanel mainpanel;
    private javax.swing.JLabel moveControl;
    private javax.swing.JTextField nameNewPlaylist;
    private javax.swing.JLabel newPlayListSubMenu;
    private javax.swing.JLabel nextSongPlayerController;
    private javax.swing.JPanel nowPlayingContainer;
    private javax.swing.JLabel nowPlayingController;
    private javax.swing.JPanel nowPlayingList;
    private javax.swing.JLabel playButtonPlayerController;
    private javax.swing.JPanel playListSubMenu;
    private javax.swing.JPanel playListSubMenuContainer;
    private javax.swing.JLabel playSearchResult;
    private javax.swing.JPanel playlistPanel;
    private javax.swing.JLabel prevSongPlayerController;
    private javax.swing.JPanel relativeArtist;
    private javax.swing.JLabel reloadButton;
    private javax.swing.JLabel repeatPlayerControl;
    private javax.swing.JScrollPane scrollAlbumPanel;
    private javax.swing.JScrollPane scrollArtistPanel;
    private javax.swing.JScrollPane scrollBarFoldersList;
    private javax.swing.JScrollPane scrollDescriptionArtist;
    private javax.swing.JScrollPane scrollGenreMenuPanel;
    private javax.swing.JScrollPane scrollGenrePanel;
    private javax.swing.JScrollPane scrollNowPlaying;
    private javax.swing.JScrollPane scrollOtherAritst;
    private javax.swing.JScrollPane scrollPlayListSubMenu;
    private javax.swing.JScrollPane scrollPlaylistPanel;
    private javax.swing.JScrollPane scrollRecentPanel;
    private javax.swing.JScrollPane scrollSongList;
    private javax.swing.JScrollPane scrollSongListSearchResult;
    private javax.swing.JTextField searchBar;
    private javax.swing.JPanel searchContainer;
    private javax.swing.JPanel searchResultPanel;
    private javax.swing.JPanel seekControl;
    private javax.swing.JPanel seekCursor;
    private javax.swing.JLabel shuffAllSongButton;
    private javax.swing.JLabel shuffPlayerControl;
    private javax.swing.JLabel shuffSearchResult;
    private javax.swing.JPanel sidebar;
    private javax.swing.JPanel songList;
    private javax.swing.JPanel songListContainer;
    private javax.swing.JPanel songListSearchResult;
    private javax.swing.JPanel suggestion;
    private javax.swing.JPanel transparentPanel;
    private javax.swing.JPanel volumeCursor;
    private javax.swing.JPanel volumeSlider;
    // End of variables declaration//GEN-END:variables
}
