/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final_prj;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/**
 *
 * @author myfar
 */
public class testScrollPanel extends javax.swing.JFrame {

    /**
     * Creates new form testScrollPanel
     */
    final Color hoverImage = new Color(24, 31, 36, 20);
    //final Color hoverImage = new Color(76, 182, 203, 90);
    public testScrollPanel() {
        initComponents();
        //jTextArea1.setBackground(new Color(0,0,0,0));
        this.setLocationRelativeTo(null);
        loadArtistPage(jPanel1);
        CustomScrollBar.setup(jScrollPane1);
        CustomScrollBar.setup(jScrollPane2);
        CustomScrollBar.setup(jScrollPane3);
        loadSongList(jPanel9);
    }
    private void loadArtistPage(JPanel container){
        JPanel artistInfor=(JPanel) container.getComponent(0);
        JPanel imageContainer=(JPanel) artistInfor.getComponent(0);
        JPanel artistNameContainer=(JPanel) artistInfor.getComponent(2);
        JLabel artistName=(JLabel) artistNameContainer.getComponent(0);
        artistName.setText("tung mini");
        JLabel image=(JLabel) imageContainer.getComponent(0);
        image.setIcon(new ImageIcon(new ImageIcon("src/Images/demo.jpg").getImage().getScaledInstance(image.getWidth(), image.getHeight(), Image.SCALE_SMOOTH)));
        jTextArea1.setText("hello\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh\nh");
    }
    private void loadSongList(JPanel container){
        JPanel[] SongItems=new JPanel[10];
        int xaxist=0;
        int yaxist=0;
        int defaultHeight=container.getHeight();
        int defaultWidth=container.getWidth();
        int totalHeight=0;
        for(int i=0; i<SongItems.length; i++){
            JPanel SongItem=new JPanel(null);
            SongItem.setSize(container.getWidth(), 30);
            SongItem.setBackground(Color.white);
            JLabel infoSong= new JLabel();
            infoSong.setSize(500,14);
            infoSong.setLocation(20, 8);
            infoSong.setText("song "+i+" - Artist"+i); 
            infoSong.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            
            JLabel iconDown=new JLabel();
            iconDown.setSize(30,30);
            iconDown.setLocation(530, 0);
            iconDown.setIcon(new ImageIcon(new ImageIcon("src/Images/icons8_Repeat_25px.png").getImage().getScaledInstance(iconDown.getWidth(), iconDown.getHeight(), Image.SCALE_SMOOTH)));
            
            JLabel iconPlay=new JLabel();
            iconPlay.setSize(30,30);
            iconPlay.setLocation(560, 0);
            
            JLabel iconAdd=new JLabel();
            iconAdd.setSize(30,30);
            iconAdd.setLocation(590,0);
            
            
            SongItem.add(infoSong);
            SongItem.add(iconDown);
            SongItem.add(iconPlay);
            SongItem.add(iconAdd);
            
            SongItem.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    SongItem.setBackground(hoverImage);
                    container.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    SongItem.setBackground(Color.white);
                    container.repaint();
                }
                
            });
            
            totalHeight+=30;
            if(totalHeight>defaultHeight){
                container.setPreferredSize(new Dimension(defaultWidth, totalHeight));
            }
            SongItem.setLocation(0,yaxist);
            container.add(SongItem);
            yaxist+=30;
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel10 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 0, 255));
        jPanel1.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(153, 153, 153));
        jPanel3.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);
        jPanel2.add(jLabel1);
        jLabel1.setBounds(0, 0, 150, 150);

        jPanel3.add(jPanel2);
        jPanel2.setBounds(0, 0, 150, 150);

        jScrollPane1.setBorder(null);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(150, 50, 470, 100);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Artist");
        jPanel7.add(jLabel2);
        jLabel2.setBounds(10, 10, 200, 30);

        jPanel3.add(jPanel7);
        jPanel7.setBounds(150, 0, 470, 50);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 0, 620, 150);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(null);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Header");
        jPanel5.add(jLabel7);
        jLabel7.setBounds(30, 14, 230, 22);

        jPanel4.add(jPanel5);
        jPanel5.setBounds(0, 0, 290, 50);

        jScrollPane3.setBorder(null);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 290, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jPanel10);

        jPanel4.add(jScrollPane3);
        jScrollPane3.setBounds(0, 50, 290, 390);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(620, 0, 290, 440);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(null);

        jPanel8.setLayout(null);

        jLabel3.setText("Song");
        jPanel8.add(jLabel3);
        jLabel3.setBounds(20, 8, 90, 14);

        jPanel6.add(jPanel8);
        jPanel8.setBounds(0, 0, 620, 30);

        jScrollPane2.setBorder(null);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jPanel11.setLayout(null);

        jLabel8.setText("jLabel8");
        jPanel11.add(jLabel8);
        jLabel8.setBounds(20, 10, 500, 14);

        jLabel9.setText("play");
        jPanel11.add(jLabel9);
        jLabel9.setBounds(560, 10, 20, 14);

        jLabel10.setText("+");
        jPanel11.add(jLabel10);
        jLabel10.setBounds(600, 10, 8, 14);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 230, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel9);

        jPanel6.add(jScrollPane2);
        jScrollPane2.setBounds(0, 30, 620, 260);

        jPanel1.add(jPanel6);
        jPanel6.setBounds(0, 150, 620, 290);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 910, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(testScrollPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(testScrollPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(testScrollPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(testScrollPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new testScrollPanel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
